package com.tms.bookingform.dao;

import com.tms.bookingform.model.BookingForm;
import com.tms.employee.model.Employee;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/30/2017.
 */
public interface IBookingFormDAO {

     BookingForm getBookingById(Integer bookingId);
     boolean saveBookingForm (BookingForm bookingForm);



}
