package com.tms.bookingform.dao;

import com.tms.bookingform.dto.ItemDTO;
import com.tms.bookingform.model.BookingComments;
import com.tms.bookingform.model.BookingForm;
import com.tms.bookingform.model.BookingInvenItem;
import com.tms.inventory.model.Inventory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by RAVI KALUARACHCHI on 11/30/2018.
 */

@Transactional
@Repository
public class BookingFormDAO implements IBookingFormDAO {


    @PersistenceContext
    private  EntityManager entityManager ;


    @Override
    public BookingForm getBookingById(Integer bookingId) {
        BookingForm bookingForm = null;
        String hql = "FROM BookingForm as b WHERE b.bookingId = :bookingId";
        List<BookingForm> bookingForms = entityManager.createQuery(hql).setParameter("bookingId", bookingId).getResultList();
        if(bookingForms != null && bookingForms.size() > 0 ) {
            bookingForm = bookingForms.get(0);
        }

        return bookingForm;
    }




    @Override
    public boolean saveBookingForm (BookingForm bookingForm) {

        BookingForm persistedBookingform = null ;
        entityManager.persist(bookingForm) ;
//        persistedBookingform = getBookingById(bookingForm.getBookingId()) ;
        if (bookingForm.getBookingId() != null)
        {

            return true;
        }
        return false;

    }


    /*//get commentby bookingId
    public BookingComments getCommentByBookingId(Integer bookingId){

        BookingComments bookingComment = null;
        String hql = "FROM BookingComments as b WHERE b.bookingId = :bookingId";
        bookingComment = entityManager.createQuery(hql).setParameter("bookingId", bookingId).getFirstResult();
        if(bookingForms != null && bookingForms.size() > 0 ) {
            bookingForm = bookingForms.get(0);
        }

        return bookingForm;


    }*/

    //Save comment
    @Override
    public boolean saveComment(BookingComments bookingComments){


        entityManager.persist(bookingComments);
        /*BookingComments persistedComment = BookingFormDAO.getCommentByBookingId(bookingComments.getBookingId());*/
        if(bookingComments.getBookingId() !=null){
            return true;
        }
        return false;
    }


    @Override
    public boolean saveBookingFormItem (BookingInvenItem bookingFormItem) {

        BookingInvenItem persistedBookingformItem = null ;
        entityManager.persist(bookingFormItem) ;
        if (bookingFormItem.getInventoryItemId() != null)
        {

            return true;
        }
        return false;

    }




    @Override
    public boolean updateBookingFormItem (BookingInvenItem bookingFormItem) {

        BookingInvenItem persistedBookingformItem = null ;
        entityManager.persist(bookingFormItem); ;
        if (bookingFormItem.getInventoryItemId() != null)
        {

            return true;
        }
        return false;

    }


// This thing for the populating filtered serial numbers for ENG form
    @Override
    public List<String> getAllINABookingInvenItemSerials () {


        String hql = "SELECT i.inventory.serial FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('INA','APPPM','APPPDGM','START','CSL') ";
        return (List<String>) entityManager.createQuery(hql).getResultList();


    }




    //getting All Booking Pending for the Approrval for program DGM

    @Override
    public List<String> getAllBookingsPending() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT Distinct i.bookingForm FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('INA') ORDER BY i.bookingForm.bookingId ";
        return (List<String>) entityManager.createQuery(hq1).getResultList();



    }

    @Override
    public List<String> getAllBookingsPendingtwo() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT Distinct i.bookingForm FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('APPPM') ORDER BY i.bookingForm.bookingId ";
        return (List<String>) entityManager.createQuery(hq1).getResultList();



    }


    @Override
    public List<String> getAllBookingsPendingthree() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT Distinct i.bookingForm FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('APPPDGM','START') ORDER BY i.bookingForm.bookingId ";
        return (List<String>) entityManager.createQuery(hq1).getResultList();



    }

    @Override
    public List<String> getBookingByIdForReports(Integer bookingId){

        String hql = "FROM BookingForm as b WHERE b.bookingId = :bookingId";
        List<String> bookingForms = entityManager.createQuery(hql).setParameter("bookingId", bookingId).getResultList();
        return bookingForms;
    }

    @Override
    public List<String> getAllCancelApproveInaBookings() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT Distinct i.bookingForm FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('CSL') ORDER BY i.bookingForm.bookingId ";
        return (List<String>) entityManager.createQuery(hq1).getResultList();


    }



    @Override
    public List<String> getAllCompletedBookings() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT Distinct i.bookingForm FROM  BookingInvenItem i where i.bookingForm.bookingStatus in ('COMPLETE') ORDER BY i.bookingForm.bookingId ";
        return (List<String>) entityManager.createQuery(hq1).getResultList();


    }



    @Override
    public boolean deleteBKItem(Integer inventoryItemId){

        try{

            BookingInvenItem existingBookingInvenItems = entityManager.find(BookingInvenItem.class,inventoryItemId);
            entityManager.remove(existingBookingInvenItems);

        }catch (Exception e){
            return false;
        }

        return true;

    }


    @Override
    public List<Integer>  getInvenItemIdByBookingId(Integer bookingId){

        String hq1 = "SELECT i.inventoryItemId FROM BookingInvenItem i where i.bookingForm.bookingId = :bookingId";

        List<Integer>  joinTablePrimaryKeyList = entityManager.createQuery(hq1).setParameter("bookingId",bookingId).getResultList();

        return joinTablePrimaryKeyList;

    }



    @Override
    public List<Inventory> getItemsById (String bookingId){

    String hql = "SELECT i.inventory FROM BookingInvenItem i where i.bookingForm.bookingId = :bookingId";
        List<Inventory> items = entityManager.createQuery(hql).setParameter("bookingId", Integer.parseInt(bookingId)).getResultList();
        return items;

    }



    @Override
    public boolean updateBookingForm(BookingForm bookingForm) {
        BookingForm persistedBookingForm = null;
        entityManager.merge(bookingForm);
        persistedBookingForm = getBookingById(bookingForm.getBookingId());
        if(persistedBookingForm != null){
            return  true;
        }
        return false;
    }


    @Override
    public List<String> getCommentsByBookingId(Integer bookingId){

        String hq1 = "SELECT c.bookingComment FROM BookingComments c where c.bookingId = :bookingId";

       List<String> comment = entityManager.createQuery(hq1).setParameter("bookingId",bookingId).getResultList();

       return comment;
    }


}
















