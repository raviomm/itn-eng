package com.tms.bookingform.model;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
@Entity
@Table (name = "booking_form")
public class BookingForm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bookform_id")
    private Integer bookingId;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name ="end_date")
    private Date endDate;
    @Column (name = "location")
    private String eventLocation;
    @Column (name = "title_of_programme")
    private String titleOfProgramme;

    public Integer getBookingId() {return bookingId;}

    public void setBookingId(Integer bookingId) {this.bookingId = bookingId;}

    public Date getStartDate() {return startDate;}

    public void setStartDate(Date startDate) {this.startDate = startDate;}

    public Date getEndDate() {return endDate;}

    public void setEndDate(Date endDate) {this.endDate = endDate;}

    public String getEventLocation() {return eventLocation;}

    public void setEventLocation(String eventLocation) {this.eventLocation = eventLocation;}

    public String getTitleOfProgramme() {return titleOfProgramme;}

    public void setTitleOfProgramme(String titleOfProgramme) {this.titleOfProgramme = titleOfProgramme;}
}
