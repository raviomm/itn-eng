package com.tms.bookingform.services;
import com.lowagie.text.pdf.AcroFields;
import com.tms.CFM.controller.bookingformController;
import com.tms.bookingform.adaptor.BookingCommentAdaptor;
import com.tms.bookingform.adaptor.BookingFormAdaptor;
import com.tms.bookingform.dao.BookingFormDAO;
import com.tms.bookingform.dto.BookingCommentsDTO;
import com.tms.bookingform.dto.BookingFormDTO;
import com.tms.bookingform.dto.ItemDTO;
import com.tms.bookingform.model.BookingComments;
import com.tms.bookingform.model.BookingForm;
import com.tms.bookingform.model.BookingInvenItem;
import com.tms.inventory.dao.InventoryDAO;
import com.tms.inventory.model.Inventory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
@Service
public  class BookingFormService implements IBookingFormService {

    @Autowired
    private BookingFormDAO bookingFormDAO;

    @Autowired
    private InventoryDAO inventoryDAO;

    @Autowired
    private bookingformController bookingformController;


    public  boolean saveBookingForm(BookingFormDTO bookingFormDTO){


        BookingFormAdaptor adaptor = new BookingFormAdaptor();
        BookingForm bookingForm = adaptor.toModel(bookingFormDTO);
        bookingFormDAO.saveBookingForm(bookingForm);
        Inventory inventory = null;
        BookingInvenItem bookingInvenItem = null;
        for(ItemDTO itemDTO : bookingFormDTO.getItems()){
            inventory =  inventoryDAO.getInventoryFromSerialNo(itemDTO.getItemCode());
            bookingInvenItem = new BookingInvenItem();
            bookingInvenItem.setInventory(inventory);
            bookingInvenItem.setBookingForm(bookingForm);
            bookingFormDAO.saveBookingFormItem(bookingInvenItem);
        }


        return true;
    }


    //Save Comment Method
    public boolean submitComment(BookingCommentsDTO bookingCommentsDTO){

        BookingCommentAdaptor adaptor = new BookingCommentAdaptor();
        BookingComments bookingComments = adaptor.toModel(bookingCommentsDTO);

        bookingFormDAO.saveComment(bookingComments);
        return true;

    }


    public  boolean updateBookingForm(BookingFormDTO bookingFormDTO){

       Integer bookingId = bookingFormDTO.getBookingId();
       BookingForm existingBookingform = bookingFormDAO.getBookingById(bookingId);
       BookingFormAdaptor adaptor = new BookingFormAdaptor();
       BookingFormDTO existingBookingDTO = adaptor.toDTO(existingBookingform);

       bookingFormDTO.startDate = existingBookingDTO.getStartDate();
       bookingFormDTO.endDate = existingBookingDTO.getEndDate();
       bookingFormDTO.eventLocation = existingBookingDTO.getEventLocation();
       bookingFormDTO.titleOfProgramme = existingBookingDTO.getTitleOfProgramme();
       bookingFormDTO.bookingStatus = existingBookingDTO.getBookingStatus();


        List<Inventory> existingItems = bookingFormDAO.getItemsById(Integer.toString(bookingId));

        BookingInvenItem existingBoookingInvenItem = null;


        Inventory existingInventory = null;

            for(Inventory itemDTO : existingItems){

                    existingInventory =  inventoryDAO.getInventoryFromSerialNo(itemDTO.getSerial());
                    existingBoookingInvenItem = new BookingInvenItem();
                    existingBoookingInvenItem.setInventory(existingInventory); // In this Line only related IDs gonna save (forign keys)
                    existingBoookingInvenItem.setBookingForm(existingBookingform); // In this Line only related IDs gonna save (forign keys)

                    List<Integer> bookingInvenItemIdList = bookingFormDAO.getInvenItemIdByBookingId(bookingId);

                    for(Integer inventoryItemId:bookingInvenItemIdList) {
                        
                        /*existingBoookingInvenItem.setInventoryItemId(inventoryItemId);*/
                        bookingFormDAO.deleteBKItem(inventoryItemId);//problem in the deleteBKItem method


                    }


                    break;

            }


        // Save data from front end starts here
        List<ItemDTO> allItemsFromFrontend = bookingFormDTO.getItems();
        BookingForm bookingForm = adaptor.toModel(bookingFormDTO);
        Inventory inventory = null;
        BookingInvenItem bookingInvenItem = null;
        for(ItemDTO itemDTO : allItemsFromFrontend){
            inventory =  inventoryDAO.getInventoryFromSerialNo(itemDTO.getSerial());
            bookingInvenItem = new BookingInvenItem();
            bookingInvenItem.setInventory(inventory); // In this Line only related IDs gonna save (forign keys)
            bookingInvenItem.setBookingForm(bookingForm); // In this Line only related IDs gonna save (forign keys)
            /*bookingInvenItem.setInventoryItemId(inventoryItemId);*/
            bookingFormDAO.saveBookingFormItem(bookingInvenItem);

        }

        return true;
    }




    @Override
    public List<String> getInventoryTypes(){

        List<String> typeList = inventoryDAO.getInventoryByType();

        return typeList;
    }


    @Override
    public List<String> getInventorySubtypes(String type){

        List<String> subTypeList = inventoryDAO.getInventoryBySubtype(type);

        return subTypeList;
        /*return inventoryDAO.getInventoryBySubtype();*/

    }

// This thing for the populating filtered serial number for ENG form
    @Override
    public List<String> getInventoryItemCodes(String subType){

        List<String > avoidList = new ArrayList<>();
        List<String> itemCodeList = inventoryDAO.getInventoryByItemCode(subType);
        List<String> actinaBookings = bookingFormDAO.getAllINABookingInvenItemSerials();

        for (String allItemCodes: itemCodeList) {

            if(!actinaBookings.contains(allItemCodes)){
                avoidList.add(allItemCodes);
            }

        }

        return avoidList;

    }


    @Override
    public Inventory getSubtypesTotalcount(String subtype){

        Inventory inventory = inventoryDAO.getSubtypesTotalcount(subtype);

        return inventory;
    }


    // This is for the programme division manager approval page
    @Override
    public List<String> getAllBookingsPending(){

        List<String> allPendingBookings = bookingFormDAO.getAllBookingsPending();

        return  allPendingBookings;

    }

    @Override
    public List<String> getBookingByIdForReports(String bookingId){

        List<String> selectedBooking = bookingFormDAO.getBookingByIdForReports(Integer.parseInt(bookingId));
        return selectedBooking;

    }


    @Override
    public List<Inventory> getItemsById(String bookingId){

        List<Inventory> itemsById = bookingFormDAO.getItemsById(bookingId);

        return itemsById;

    }


// For Programme Manager Approve Button

    @Override
    public boolean statusApprovePm(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("INA")){
                existingBookingForm.setBookingStatus("APPPM");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }


    // For Programme Manager Cancel Button
    @Override
    public boolean statusCancelPm(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("INA")){
                existingBookingForm.setBookingStatus("CSL");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }


    // This is for the programme division manager approval page
    @Override
    public List<String> getAllBookingsPendingtwo(){

        List<String> allPendingBookings = bookingFormDAO.getAllBookingsPendingtwo();

        return  allPendingBookings;

    }

    // This is for the programme division manager approval page
    @Override
    public List<String> getAllBookingsPendingthree(){

        List<String> allPendingBookings = bookingFormDAO.getAllBookingsPendingthree();

        return  allPendingBookings;

    }

    // This is for the Reservation Edit page
    @Override
    public List<String> getAllBookingsPendingReservationEdit(){

        List<String> allCancelApproveBookings = bookingFormDAO.getAllCancelApproveInaBookings();

        return  allCancelApproveBookings;

    }

   //for reporting page main table populate
    @Override
    public List<String> getAllBookingsCompleted(){

        List<String> allCancelApproveBookings = bookingFormDAO.getAllCompletedBookings();

        return  allCancelApproveBookings;

    }

    // For Programme DGM Approve Button

    @Override
    public boolean statusApprovePmtwo(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("APPPM")){
                existingBookingForm.setBookingStatus("APPPDGM");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }

    // For Programme DGM Cancel Button
    @Override
    public boolean statusCancelPmtwo(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("APPPM")){
                existingBookingForm.setBookingStatus("CSL");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }

// Start Button ENG Final Page
    @Override
    public boolean statusStartEng(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("APPPDGM")){
                existingBookingForm.setBookingStatus("START");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }

    // For Programme DGM Cancel Button
    @Override
    public boolean statusCompleteEng(BookingFormDTO bookingFormDTO) {

        BookingForm existingBookingForm = bookingFormDAO.getBookingById(bookingFormDTO.getBookingId());
        if(existingBookingForm != null){
            if(existingBookingForm.getBookingStatus().equals("START")){
                existingBookingForm.setBookingStatus("COMPLETE");
                return bookingFormDAO.updateBookingForm(existingBookingForm);
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public String getCommentsByBookingId(String bookingId){

        List<String> selectedBooking = bookingFormDAO.getCommentsByBookingId(Integer.parseInt(bookingId));

        // create object of StringBuilder class
        StringBuilder sb = new StringBuilder();

        // Appends characters one by one
        for (String ch : selectedBooking) {
            sb.append(ch);
        }

        // convert in string
        String comment = sb.toString();

        return comment;

    }

}
