package com.tms.bookingform.services;

import com.tms.bookingform.dto.BookingFormDTO;
import com.tms.inventory.model.Inventory;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
public interface IBookingFormService {

    boolean saveBookingForm(BookingFormDTO bookingFormDTO);
    List<String> getInventoryTypes();
    List<String> getInventorySubtypes(String type);
    Inventory getSubtypesTotalcount(String subtype);



}
