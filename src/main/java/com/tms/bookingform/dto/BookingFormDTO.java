package com.tms.bookingform.dto;

import java.io.Serializable;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
public class BookingFormDTO  {

    /*private static final long serialVersionUID = 7526472295222376142L;*/


    private Integer bookingId;
    private String startDate;
    private String endDate;
    private String eventLocation;
    private String titleOfProgramme;


    public Integer getBookingId() {return bookingId;}

    public void setBookingId(Integer bookingId) {this.bookingId = bookingId;}

    public String getStartDate() {return startDate;}

    public void setStartDate(String startDate) {this.startDate = startDate;}

    public String getEndDate() {return endDate;}

    public void setEndDate(String endDate) {this.endDate = endDate;}

    public String getEventLocation() {return eventLocation;}

    public void setEventLocation(String eventLocation) {this.eventLocation = eventLocation;}

    public String getTitleOfProgramme() {return titleOfProgramme;}

    public void setTitleOfProgramme(String titleOfProgramme) {this.titleOfProgramme = titleOfProgramme;}



}
