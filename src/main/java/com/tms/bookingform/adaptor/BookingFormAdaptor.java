package com.tms.bookingform.adaptor;

import com.tms.bookingform.dto.BookingFormDTO;
import com.tms.bookingform.model.BookingForm;
import com.tms.common.adaptor.BasicAdaptor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
public class BookingFormAdaptor extends BasicAdaptor <BookingForm,BookingFormDTO> {

@Override
    public BookingForm toModel (BookingFormDTO dto){

    BookingForm boooking = new BookingForm();

    boooking.setBookingId(dto.getBookingId());
    DateFormat formatter = new SimpleDateFormat("HH:mm");
//Start date
    Date date1= null;
    try {
        date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getStartDate());
    } catch (ParseException e) {
        e.printStackTrace();
    }
    boooking.setStartDate(date1);

 //End date
    Date date2= null;
    try {
        date2 = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getEndDate());
    } catch (ParseException e) {
        e.printStackTrace();
    }
    boooking.setEndDate(date2);

    boooking.setEventLocation(dto.getEventLocation());
    boooking.setTitleOfProgramme(dto.getTitleOfProgramme());
    return boooking;
}

    @Override
    public BookingFormDTO toDTO (BookingForm model){

        BookingFormDTO dto = new BookingFormDTO();

        dto.setBookingId(model.getBookingId());
        dto.setStartDate(model.getStartDate().toString());
        dto.setEndDate(model.getEndDate().toString());
        dto.setEventLocation(model.getEventLocation());
        dto.setTitleOfProgramme(model.getTitleOfProgramme());
        return dto;

    }


}
