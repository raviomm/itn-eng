package com.tms.fleet.services;

import com.tms.employee.dto.EmployeeDTO;
import com.tms.fleet.dto.FleetDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */
public interface IFleetService {

     boolean saveFleet(FleetDTO fleetDTO);
    List<FleetDTO> getAllFleets();
    FleetDTO getFleetByName(String fleetName);
    boolean updateFleet(FleetDTO fleetDTO);
    boolean deleteFleet(FleetDTO fleetDTO);
    List<EmployeeDTO> getUsableDrivers();
    List<EmployeeDTO> getUsableHelpers();

}
