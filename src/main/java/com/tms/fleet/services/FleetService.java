package com.tms.fleet.services;


import com.tms.employee.dto.EmployeeDTO;
import com.tms.employee.model.Employee;
import com.tms.employee.services.EmployeeService;
import com.tms.employee.services.IEmployeeService;
import com.tms.fleet.adaptor.FleetAdaptor;
import com.tms.fleet.dao.IFleetDAO;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.model.Fleet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */

@Service
public class FleetService implements IFleetService {


    @Autowired
    private IFleetDAO fleetDAO;

    @Autowired
    private IEmployeeService employeeServices;

    @Override
    public List<FleetDTO> getAllFleets() {
        FleetAdaptor adaptor = new FleetAdaptor();
        List<FleetDTO> dtoList = new ArrayList<>();
        List<Fleet> fleetList = fleetDAO.getAllActInaFleets();
        if(fleetList != null & !fleetList.isEmpty()){
            for(Fleet fleet : fleetList){
                dtoList.add(adaptor.toDTO(fleet));
            }
        }
        return dtoList;
    }


    @Override
    public  List<EmployeeDTO> getUsableDrivers(){


        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<EmployeeDTO> driverList = employeeServices.getAllDrivers();
        List<Fleet> activeDrivers = fleetDAO.getUsableDrivers();

        if(activeDrivers.size() >0) {
            for (EmployeeDTO driverD : driverList) {

                for (Fleet aDriver : activeDrivers) {

                    if (!driverD.getEmployeeId().equals(aDriver.getSelectDriver())) {

                        dtoList.add(driverD);
                    }
                }
            }
        }else {
            return driverList;
        }

        return dtoList;
    }

    @Override
    public  List<EmployeeDTO> getUsableHelpers(){


        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<EmployeeDTO> helperList = employeeServices.getAllHelpers();
        List<Fleet> activeHelpers = fleetDAO.getUsableHelpers();

        if(activeHelpers.size() >0) {
            for (EmployeeDTO helperH : helperList) {

                for (Fleet aHelper : activeHelpers) {

                    if (!helperH.getEmployeeId().equals(aHelper.getSelectHelper())) {

                        dtoList.add(helperH);
                    }
                }
            }
        }else {
            return helperList;
        }

        return dtoList;
    }



    @Override
    public FleetDTO getFleetByName(String fleetName) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = fleetDAO.getFleetByName(fleetName);
        if(fleet != null){
            return adaptor.toDTO(fleet);
        } else {
            return null;
        }
    }



    @Override
    public boolean saveFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = adaptor.toModel(fleetDTO);
        return fleetDAO.saveFleet(fleet);
    }


    @Override
    public boolean updateFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = adaptor.toModel(fleetDTO);
        Fleet existingFleet = fleetDAO.getFleetById(fleet.getFleetId());
        if(existingFleet != null){
            //existingFleet.setRouteOrigin(route.getRouteOrigin());
            //existingFleet.setRouteDestination(route.getRouteDestination());
            //existingFleet.setRouteStatus(route.getRouteStatus());
            return fleetDAO.updateFleet(fleet);
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = adaptor.toModel(fleetDTO);
        Fleet existingFleet = fleetDAO.getFleetById(fleet.getFleetId());
        if(existingFleet != null){
            return fleetDAO.deleteFleet(existingFleet);
        } else {
            return false;
        }
    }



}
