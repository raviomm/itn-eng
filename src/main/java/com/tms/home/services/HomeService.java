package com.tms.home.services;

import com.tms.employee.services.IEmployeeService;
import com.tms.fleet.adaptor.FleetAdaptor;
import com.tms.fleet.dao.IFleetDAO;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.model.Fleet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 4/3/2018.
 */
@Service
public class HomeService implements IHomeService {

    @Autowired
    private IFleetDAO fleetDAO;

    @Autowired
    private IEmployeeService employeeServices;

    @Override
    public List<FleetDTO> getAllActInaFleets() {
        FleetAdaptor adaptor = new FleetAdaptor();
        List<FleetDTO> dtoList = new ArrayList<>();
        List<Fleet> fleetList1 = fleetDAO.getAllActInaFleets();
        if (fleetList1 != null & !fleetList1.isEmpty()) {
            for (Fleet fleet : fleetList1) {
                dtoList.add(adaptor.toDTO(fleet));
            }
        }
        return dtoList;
    }


    @Override
    public FleetDTO getFleetByName(String fleetName) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = fleetDAO.getFleetByName(fleetName);
        if (fleet != null) {
            return adaptor.toDTO(fleet);
        } else {
            return null;
        }
    }

    @Override
    public boolean changeStatus(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet existingFleet = fleetDAO.getFleetById(fleetDTO.getFleetId());
        if(existingFleet != null){
            if(existingFleet.getFleetStatus().equals("ACT")){
                existingFleet.setFleetStatus("INA");
            } else {
                existingFleet.setFleetStatus("ACT");
            }

            return fleetDAO.updateFleet(existingFleet);
        } else {
            return false;
        }
    }

    @Override
    public boolean statusComplete(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet existingFleet = fleetDAO.getFleetById(fleetDTO.getFleetId());
        if(existingFleet != null){
            if(existingFleet.getFleetStatus().equals("ACT")){
                existingFleet.setFleetStatus("CML");
                return fleetDAO.updateFleet(existingFleet);
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean statusCancel(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet existingFleet = fleetDAO.getFleetById(fleetDTO.getFleetId());
        if(existingFleet != null){
            if(existingFleet.getFleetStatus().equals("ACT")){
                existingFleet.setFleetStatus("CSL");
                return fleetDAO.updateFleet(existingFleet);
            }
            return false;

        } else {
            return false;
        }
    }

}