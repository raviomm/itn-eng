package com.tms.home.services;

import com.tms.fleet.dto.FleetDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 4/3/2018.
 */
public interface IHomeService {

     List<FleetDTO> getAllActInaFleets();

     FleetDTO getFleetByName(String fleetName);

     boolean changeStatus(FleetDTO fleetDTO);
     boolean statusComplete(FleetDTO fleetDTO);
     boolean statusCancel(FleetDTO fleetDTO);

}
