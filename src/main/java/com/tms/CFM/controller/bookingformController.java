package com.tms.CFM.controller;

import com.tms.bookingform.dto.BookingFormDTO;
import com.tms.bookingform.services.IBookingFormService;
import com.tms.common.dto.Response;
import com.tms.inventory.dto.InventoryDTO;
import com.tms.inventory.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
@Controller
@RequestMapping ("/bookingForm")
public class bookingformController {


    @Autowired
    private IBookingFormService bookingFormService;

    @RequestMapping(value = "/saveBookingForm", method = RequestMethod.POST)
    @ResponseBody
    public Response saveBookingform(final BookingFormDTO bookingFormDTO) {
        Response response = new Response();
        boolean saveSuccess = bookingFormService.saveBookingForm(bookingFormDTO);
        response.setSuccess(saveSuccess);
        return response;

    }


    @RequestMapping(value = "/getAllTypes", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getUsableTypes(){

        List<String> types = new ArrayList<>();
            List<String> typeList = bookingFormService.getInventoryTypes();
            if(typeList != null){

                types.addAll(typeList);
            }

        return types;
    }


    @RequestMapping (value = "/getSubType" , method = RequestMethod.GET)
    @ResponseBody
    public List<String> getRelatedSubtypes(@RequestParam String type)

    {
        List<String> subTypes = new ArrayList<>();
        List<String> SubTypeList = bookingFormService.getInventorySubtypes(type);
        if(SubTypeList != null){

            subTypes.addAll(SubTypeList);
        }

        return subTypes;


    }


    @RequestMapping (value = "/getTotalCount" , method = RequestMethod.GET)
    @ResponseBody
    public Inventory getTotalCount (@RequestParam String subtype){

        Inventory inventory = bookingFormService.getSubtypesTotalcount(subtype);

        return inventory;

        
    }


}
