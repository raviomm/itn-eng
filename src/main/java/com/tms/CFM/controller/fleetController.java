package com.tms.CFM.controller;

import com.tms.common.dto.Response;
import com.tms.employee.dto.EmployeeDTO;
import com.tms.employee.model.Employee;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.services.IFleetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 12/14/2017.
 */
@Controller
@RequestMapping ("/fleet")

public class fleetController {

    @Autowired
    private IFleetService fleetService;

    @RequestMapping(value = "/saveFleetRecord", method = RequestMethod.POST)
    @ResponseBody
    public Response saveFleet(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = fleetService.saveFleet(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;

    }

    @RequestMapping(value = "/getAllFleets", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllFleets(String fleetName) {
        Response<FleetDTO> response = new Response();
        List<FleetDTO> fleets = new ArrayList<>();
        if(fleetName !=  null && fleetName != ""){
            FleetDTO fleet = fleetService.getFleetByName(fleetName);
            if(fleet != null){
                fleets.add(fleet);
            }
        } else {
            fleets = fleetService.getAllFleets();
        }

        response.setTableData(fleets);
        response.setSuccess(true);
        return response;

    }

    // Available Drivers loading to drop down
    @RequestMapping(value = "/getAllDrivers", method = RequestMethod.POST)
    @ResponseBody
    public Response getUsableDrivers(EmployeeDTO employeeName){

        Response<EmployeeDTO> response = new Response<>();
        List<EmployeeDTO> drivers = new ArrayList<>();
        if(employeeName != null){
            List<EmployeeDTO> nameD = fleetService.getUsableDrivers();
            if(nameD != null){

                drivers.addAll(nameD);

            }
        }else {
            return null;
        }

        response.setTableData(drivers);
        response.setSuccess(true);
        return response;
        }



    @RequestMapping(value = "/getAllHelpers", method = RequestMethod.POST)
    @ResponseBody
    public Response getUsableHelpers(EmployeeDTO employeeName){

        Response<EmployeeDTO> response = new Response<>();
        List<EmployeeDTO> helpers = new ArrayList<>();
        if(employeeName != null){
            List<EmployeeDTO> nameD = fleetService.getUsableHelpers();
            if(nameD != null){

                helpers.addAll(nameD);

            }
        }else {
            return null;
        }

        response.setTableData(helpers);
        response.setSuccess(true);
        return response;
    }



    @RequestMapping(value = "/updateFleet", method = RequestMethod.POST)
    @ResponseBody
    public Response updateFleet(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = fleetService.updateFleet(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/deleteFleet", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteFleet(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = fleetService.deleteFleet(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;
    }




}
