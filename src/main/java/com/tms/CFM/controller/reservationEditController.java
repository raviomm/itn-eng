package com.tms.CFM.controller;
import com.tms.bookingform.dto.BookingFormDTO;
import com.tms.bookingform.services.IBookingFormService;
import com.tms.common.dto.Response;
import com.tms.inventory.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 4/30/2019.
 */

@Controller
@RequestMapping("/reservationEdit")


public class reservationEditController {

    @Autowired
    private IBookingFormService bookingFormService;


    //// This is for the programme division manager approval page

    @RequestMapping(value = "/getAllEngs", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllCancelApproveBookings(){

        Response<String> response = new Response();


        List<String> allBookingsPending = bookingFormService.getAllBookingsPendingReservationEdit();

        response.setTableData(allBookingsPending);
        response.setSuccess(true);
        return response;



    }



    @RequestMapping(value = "/getAllEngItems", method = RequestMethod.GET)
    @ResponseBody
    public Response<Inventory> getAllEngItems(@RequestParam String bookingId) {

        Response<Inventory> response = new Response();
        List<Inventory> engItems = new ArrayList<>();
        if (bookingId != null && bookingId != "" ) {
            engItems = bookingFormService.getItemsById(bookingId);
        }
        response.setTableData(engItems);
        response.setSuccess(true);
        return response;
    }




    @RequestMapping(value = "/updateBookingForm", method = RequestMethod.POST)
    @ResponseBody
    public Response updateBookingform(@RequestBody BookingFormDTO bookingFormDTO) {
        Response response = new Response();
        boolean saveSuccess = bookingFormService.updateBookingForm(bookingFormDTO);
        response.setSuccess(saveSuccess);
        return response;

    }


}
