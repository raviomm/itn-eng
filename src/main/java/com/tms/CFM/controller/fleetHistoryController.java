package com.tms.CFM.controller;

import com.tms.common.dto.Response;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.services.IFleetService;
import com.tms.fleethistory.IFleetHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 6/11/2018.
 */
@Controller
@RequestMapping ("/fleetHistory")
public class fleetHistoryController {

    @Autowired IFleetHistoryService fleetHistoryService;
    @Autowired IFleetService fleetService;


    @RequestMapping(value = "/getAllFleetHistoryTable", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllFleetHistoryTable(String fleetName) {

        Response<FleetDTO> response = new Response();
        List<FleetDTO> fleets = new ArrayList<>();
        if(fleetName !=  null && fleetName != ""){
            FleetDTO fleet = fleetService.getFleetByName(fleetName);
            if(fleet != null){
                fleets.add(fleet);
            }
        } else {
            fleets = fleetHistoryService.getAllCompCanFleets();
        }
        response.setTableData(fleets);
        response.setSuccess(true);
        return response;
    }

}
