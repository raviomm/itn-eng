package com.tms.CFM.controller;

import com.tms.common.dto.Response;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.services.IFleetService;
import com.tms.home.services.HomeService;
import com.tms.home.services.IHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 3/16/2018.
 */
@Controller
@RequestMapping("/home")
public class homeController {

    @Autowired
    private IHomeService homeService;

    @RequestMapping(value = "/getAllHometable", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllHometable(String fleetName) {

        Response<FleetDTO> response = new Response();
        List<FleetDTO> fleets = new ArrayList<>();
        if(fleetName !=  null && fleetName != ""){
            FleetDTO fleet = homeService.getFleetByName(fleetName);
            if(fleet != null){
                fleets.add(fleet);
            }
        } else {
            fleets = homeService.getAllActInaFleets();
        }
        response.setTableData(fleets);
        response.setSuccess(true);
        return response;
    }

    @RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
    @ResponseBody
    public Response updateFleet(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = homeService.changeStatus(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/statusComplete", method = RequestMethod.POST)
    @ResponseBody
    public Response statusComplete(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = homeService.statusComplete(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/statusCancel", method = RequestMethod.POST)
    @ResponseBody
    public Response statusCancel(final FleetDTO fleetDTO) {
        Response response = new Response();
        boolean saveSuccess = homeService.statusCancel(fleetDTO);
        response.setSuccess(saveSuccess);
        return response;
    }
}
