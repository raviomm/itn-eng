package com.tms.fleethistory;

import com.tms.fleet.adaptor.FleetAdaptor;
import com.tms.fleet.dao.FleetDAO;
import com.tms.fleet.dao.IFleetDAO;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.model.Fleet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 6/11/2018.
 */
@Service
public class FleetHistoryService implements IFleetHistoryService {

    @Autowired
    private IFleetDAO fleetDAO;

    @Override
    public List<FleetDTO> getAllCompCanFleets() {
        FleetAdaptor adaptor = new FleetAdaptor();
        List<FleetDTO> dtoList = new ArrayList<>();
        List<Fleet> fleetList1 = fleetDAO.getAllCompCanFleets();
        if (fleetList1 != null & !fleetList1.isEmpty()) {
            for (Fleet fleet : fleetList1) {
                dtoList.add(adaptor.toDTO(fleet));
            }
        }
        return dtoList;
    }


}
