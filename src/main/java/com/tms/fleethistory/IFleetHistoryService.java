package com.tms.fleethistory;

import com.tms.fleet.dto.FleetDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 6/11/2018.
 */
public interface IFleetHistoryService {

    List<FleetDTO> getAllCompCanFleets();
}
