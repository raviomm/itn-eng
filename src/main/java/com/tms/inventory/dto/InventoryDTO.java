package com.tms.inventory.dto;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by RAVI KALUARACHCHI on 7/15/2018.
 */
public class InventoryDTO {

    private Integer inventoryId;
    private String type;
    private String subType;
    private Integer totalCount;
    private Integer availableCount;

    public Integer getInventoryId() { return inventoryId; }

    public void setInventoryId(Integer inventoryId) { this.inventoryId = inventoryId; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getSubType() { return subType; }

    public void setSubType(String subType) { this.subType = subType; }

    public Integer getTotalCount() { return totalCount; }

    public void setTotalCount(Integer totalCount) { this.totalCount = totalCount;}

    public Integer getAvailableCount() { return availableCount; }

    public void setAvailableCount(Integer availableCount) { this.availableCount = availableCount; }

}
