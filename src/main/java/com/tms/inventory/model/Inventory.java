package com.tms.inventory.model;

import javax.persistence.*;

/**
 * Created by RAVI KALUARACHCHI on 7/14/2018.
 */
@Entity
@Table (name = "inventory")
public class Inventory {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column (name = "inventory_id")
    private Integer inventoryId;
    @Column(name = "type")
    private String type;
    @Column (name = "sub_type")
    private String subType;
    @Column (name = "total_count")
    private Integer totalCount;
    @Column (name = "available_count")
    private Integer availableCount;


    public Integer getInventoryId() { return inventoryId;}

    public void setInventoryId(Integer inventoryId) { this.inventoryId = inventoryId; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getSubType() { return subType; }

    public void setSubType(String subType) { this.subType = subType; }

    public Integer getTotalCount() { return totalCount; }

    public void setTotalCount(Integer totalCount) { this.totalCount = totalCount; }

    public Integer getAvailableCount() { return availableCount; }

    public void setAvailableCount(Integer availableCount) { this.availableCount = availableCount; }
}
