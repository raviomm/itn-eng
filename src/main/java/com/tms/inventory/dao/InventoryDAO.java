package com.tms.inventory.dao;

import com.tms.bookingform.services.BookingFormService;
import com.tms.inventory.model.Inventory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 7/14/2018.
 */
@Transactional
@Repository
public class InventoryDAO implements IInventoryDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<String> getInventoryByType() {

        List<String> typeList = new ArrayList<>();
        String hql = "SELECT Distinct i.type From Inventory i";
        typeList = entityManager.createQuery(hql).getResultList();
        return  typeList;

    }

    @Override
    public List<String> getInventoryBySubtype (String type) {

        List<String> subTypeList = new ArrayList<>();
        String hql = "SELECT Distinct i.subType From Inventory i Where i.type = :type ";
        subTypeList = entityManager.createQuery(hql).setParameter("type",type).getResultList();
        return  subTypeList;

        /*String hq2 = "SELECT i.subType FROM Inventory i WHERE i.type = type ";*/
        /*subtypeList = entityManager.createQuery(hq2).getResultList();*/
    }

    @Override
    public Inventory getSubtypesTotalcount(String subtype){

        Inventory inventoryObject;
        String hq1 = "SELECT i From Inventory i Where i.subType = :subtype ";
        inventoryObject = (Inventory)entityManager.createQuery(hq1).setParameter("subtype",subtype).getSingleResult();
        return inventoryObject;

    }




}
