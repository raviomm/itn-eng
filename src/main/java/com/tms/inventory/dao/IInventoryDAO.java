package com.tms.inventory.dao;

import com.tms.inventory.model.Inventory;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 7/14/2018.
 */
public interface IInventoryDAO {

    List<String> getInventoryByType();
    List<String> getInventoryBySubtype (String type);
    Inventory getSubtypesTotalcount(String subtype);


}
