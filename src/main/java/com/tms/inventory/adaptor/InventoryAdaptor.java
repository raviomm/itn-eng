package com.tms.inventory.adaptor;

import com.tms.common.adaptor.BasicAdaptor;
import com.tms.inventory.dto.InventoryDTO;
import com.tms.inventory.model.Inventory;

/**
 * Created by RAVI KALUARACHCHI on 7/15/2018.
 */
public class InventoryAdaptor extends BasicAdaptor <Inventory,InventoryDTO> {

    @Override
    public Inventory toModel (InventoryDTO dto){

        Inventory inventory = new Inventory();
        inventory.setInventoryId(dto.getInventoryId());
        inventory.setType(dto.getType());
        inventory.setSubType(dto.getSubType());
        inventory.setTotalCount(dto.getTotalCount());
        inventory.setAvailableCount(dto.getAvailableCount());

        return inventory;
    }

    @Override
    public InventoryDTO toDTO (Inventory model){

        InventoryDTO dto = new InventoryDTO();
        dto.setInventoryId(model.getInventoryId());
        dto.setType(model.getType());
        dto.setSubType(model.getSubType());
        dto.setTotalCount(model.getTotalCount());
        dto.setAvailableCount(model.getAvailableCount());

        return dto;

    }



}
