<html lang="en"><head>
    




    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">




    <!--  time picker needs     -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">

    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>
</head>
<body style="">
<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Sithumina Transport
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li>
                <a href="user-accout.html">
                    <i class="pe-7s-user"></i>
                    <p>User Accounts</p>
                </a>
            </li>
            <li>
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li></li><li class="">
            <a href="manageVehicle.html">
                <i class="pe-7s-note2"></i>
                <p>Vehicle</p>
            </a>
        </li>
            <li >
                <a href="manageRoute.html">
                    <i class="pe-7s-news-paper"></i>
                    <p>Route</p>
                </a>
            </li>
            <li>
                <a href="manageProduct.html">
                    <i class="pe-7s-science"></i>
                    <p>Product</p>
                </a>
            </li>
            <li class="active">
                <a href="manageFleet.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Fleets</p>
                </a>
            </li>
            <li>
                <a href="manageFleetHistory.html">
                    <i class="pe-7s-graph"></i>
                    <p>Fleet History</p>
                </a>
            </li>
            <li>
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
            <li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div></div>

<div class="wrapper">
    <div class="main-panel">


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Fleet Management Page</h4>
                            </div>

                            <div class="content">
                                <form id="fleetForm">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="pull-left">
                                                    <input id="fleetId" type="text" hidden>
                                                    <h5>Fleet Name</h5>
                                                    <input id="fleetName" type="text" class="form-control2" placeholder="Enter fleet name" value="">
                                                    <br>
                                                    <div class="dropdown">
                                                        <h5>Select Route</h5>
                                                        <select id="selectRoute" class="btn btn-default dropdown-toggle" >
                                                        </select>
                                                    </div>
                                                    <div class="dropdown">
                                                        <h5>Select Product</h5>&nbsp &nbsp &nbsp
                                                        <select id="selectProduct" class="btn btn-default dropdown-toggle" >
                                                        </select>
                                                    </div>
                                                    <div class="dropdown">
                                                        <h5>Select Vehicle</h5>&nbsp &nbsp &nbsp
                                                        <select id="selectVehicle" class="btn btn-default dropdown-toggle" >
                                                        </select>
                                                    </div>
                                                    <div class="dropdown">
                                                        <h5>Select Driver</h5>&nbsp &nbsp &nbsp
                                                        <select id="selectDriver" class="btn btn-default dropdown-toggle" >
                                                        </select>
                                                    </div>
                                                    <div class="dropdown">
                                                        <h5>Select Helper</h5>&nbsp &nbsp &nbsp
                                                        <select id="selectHelper" class="btn btn-default dropdown-toggle" >
                                                        </select>
                                                    </div>
                                                    <br>
                                                    <h5>Start Time</h5>
                                                    <input id="fleetStartTime" type="time" class="form-control2" >
                                                    <br>
                                                    <h5>Petty Cash </h5>
                                                    <input id="fleetPettyCash" type="text" class="form-control2" placeholder="Enter petty cash amount" value="">
                                                    <br>
                                                    <h5>Petty Cash Description</h5>
                                                    <input id="fleetPettyCashDescription" type="text" class="form-control2" placeholder="Enter petty cash description" value="">
                                                    <br>
                                                    <h5>Due Date</h5>
                                                    <input type="date" id="fleetDueDate" class="form-control2" >
                                                    <br>
                                                    <br>
                                                    <input id="fleetStatus" value="INA" type="text" hidden>
                                                    <button type="submit" id="fleetSaveButton" onclick="fleetSave()" class="btn btn-info btn-fill pull-left">Save</button>
                                                    <button type="submit" id="fleetUpdateButton" onclick="fleetUpdate()" class="btn btn-info btn-fill pull-left">Update</button>
                                                    <br>
                                                    <br>
                                                    <div class="clearfix"></div>

                                                </div>
                                                <br> <br>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                </form>
                            </div>
                        </div>
                    </div>




                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                <div class="pull-center">
                                    <h5>Search Fleet</h5>
                                    <input type="text" id="searchFleetName" class="form-control1" placeholder="TYPE YOUR FLEET NAME HERE" name="search"><br>
                                    <button type="submit" onclick="searchFleet()" class="btn btn-info btn-fill pull-center">Search</button>&nbsp &nbsp &nbsp


                                    <br>
                                </div><br><br>

                                <div class="content table-responsive table-full-width">
                                    <table id="fleetTable" class="display" width="100%"></table>

                                    <br>
                                    <br>
                                    <div class="pull-center">
                                        <button type="submit" id="fleetAddButton" onclick="fleetAdd()" class="btn btn-info btn-fill pull-center">Add</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="fleetEditButton" onclick="fleetEdit()" class="btn btn-info btn-fill pull-center">Edit</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="fleetDeleteButton" onclick="fleetDelete()" class="btn btn-info btn-fill pull-center">Delete</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="fleetClearButton" onclick="fleetClear()" class="btn btn-info btn-fill pull-center">Clear</button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <footer class="footer">

            </footer>

        </div>
    </div>






    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <!-- JQuery data table adding -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <!-- Include the js file to the page-->
    <script src="assets/js/custom/manageFleet.js"></script>


</div>
</body></html>