<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <title>Spring Security Example</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" type="text/javascript"></script>



</head>
<body >

</br>
</br>
<h3 style="font-size:30px;"  align="center">ITN ENG</h3>
<div class="container-fluid"    align="center"  style="background-color: Orange; width: 50%; margin:auto;" >

    <c:if test="${param.error ne null}">
        <div style="color: white">Invalid credentials.</div>
    </c:if>
    <form action="/login" method="post">
        <div align="center" class="card" style="background-color: #800000;width: 90%;"  >
            <br>
            <label for="username"><h4 style="color:#FFFAF0">UserName:</h4>
                <input type="text" class="form-control2" id="username" name="username"  maxlength="30" size="30">
            </label>
        <br><br>

            <label for="pwd"><h4 style="color:#FFFAF0">Password:</h4>
                <input type="password" class="form-control2" id="pwd" name="password"   maxlength="30" size="30">
            </label>
            <br>
            <br>
            <button type="submit" class="btn btn-info btn-fill pull-left" align="center" style="width: 20%;">Submit</button>
            <br>
        </div>


        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>
</div>


</body>
</html>