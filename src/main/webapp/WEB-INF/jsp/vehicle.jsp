<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Light Bootstrap Dashboard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
	
	
    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
	<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
		<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Sithumina Transport
                </a>
            </div>

            <ul class="nav">
                <li class="deactive">
                    <a href="manageHome.html">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
				<li>
                    <a href="../../../../../../../ravi/light-bootstrap-dashboard-master-edit-3/user-accout.html">
                        <i class="pe-7s-user"></i>
                        <p>User Accounts</p>
                    </a>
                </li>
                <li class="">
                    <a href="manageEmployee.html">
                        <i class="pe-7s-user"></i>
                        <p>Employee</p>
                    </a>
                </li>
                <li><li class="active">
                    <a href="manageVehicle.html">
                        <i class="pe-7s-note2"></i>
                        <p>Vehicle</p>
                    </a>
                </li></li>
                <li>
                    <a href="manageRoute.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>Route</p>
                    </a>
                </li>
                <li>
                    <a href="manageProduct.html">
                        <i class="pe-7s-science"></i>
                        <p>Product</p>
                    </a>
                </li>
                <li>
                    <a href="manageFleet.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Fleets</p>
                    </a>
                </li>
                <li>
                    <a href="manageFleetHistory.html">
                        <i class="pe-7s-graph"></i>
                        <p>Fleet History</p>
                    </a>
                </li>
                <li>
                    <a href="manageSalary.html">
                        <i class="pe-7s-note2"></i>
                        <p>Salary Calculation</p>
                    </a>
                </li>
				<li>
                    <a href="manageReport.html">
                        <i class="pe-7s-note2"></i>
                        <p>Report Generator</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="../../../../../../../ravi/light-bootstrap-dashboard-master-edit-3/upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

<div class="wrapper">
	<div class="main-panel">

         <div class="content"> 
              <div class="container-fluid"> 
                <div class="row"> 
                    <div class="col-md-4">
                         <div class="card">  
                            <div class="header">
                                <h4 class="title">Vehicle Information Management</h4><br>
                            </div>
                            <div class="content">
                                <form id="vehicleForm">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="pull-left">
                                                <input id="id" type="text" hidden>
												<h5>Reg No</h5>
                                                <input id="vehcleRegNo" type="text" class="form-control2" placeholder="xx-1234" value="">
                                                <br>
												<div class="dropdown">
													<h5>Vehicle Type</h5>&nbsp &nbsp &nbsp
                                                    <select id="vehicleType" class="btn btn-default dropdown-toggle" >
                                                        <option value=""></option>
                                                        <option value="CAR">Car</option>
                                                        <option value="TRUCK">Truck</option>
                                                        <option value="LORRY">Lorry</option>
                                                        <option value="VAN">Van</option>
                                                    </select>
                                                </div>

												<div class="dropdown">
													<h5>Capacity</h5> &nbsp &nbsp &nbsp
                                                    <select id="vehicleCapacity" class="btn btn-default dropdown-toggle" >
                                                        <option value=""></option>
                                                        <option value="2000">2000 Kg</option>
                                                        <option value="1000">1000 Kg</option>
                                                        <option value="500">500 Kg</option>
                                                        <option value="250">250 Kg</option>
                                                    </select>

                                                </div>

												<div class="dropdown">
													<h5>Fuel Type</h5> &nbsp &nbsp &nbsp
                                                    <select id="vehicleFuelType" class="btn btn-default dropdown-toggle" >
                                                        <option value=""></option>
                                                        <option value="P">Petrol</option>
                                                        <option value="D">Diesel</option>
                                                        <option value="H">Hybride</option>
                                                        <option value="E">Electric</option>
                                                    </select>
                                                </div>

												<div class="dropdown">
													<h5>Owner</h5>&nbsp &nbsp &nbsp
                                                    <select id="vehicleUser" class="btn btn-default dropdown-toggle" >
                                                        <option value=""></option>
                                                        <option value="RAVI">RAVI</option>
                                                        <option value="COMPANY">COMPANY</option>
                                                        <option value="KALUM">KALUM</option>
                                                        <option value="USER1">USER1</option>
                                                    </select>

													</div>
												<br>
												<h5>Chassis No</h5>
                                                <input id="vehicleChassisNo" type="text" class="form-control2" placeholder="0123456789XX" value="">
												<br>
												<h5>Engine No</h5>
                                                <input id="vehicleEngineNo" type="text" class="form-control2" placeholder="0123456789PP" value="">
												<br>
												<h5>Model</h5>
                                                <input id="vehicleModel" type="text" class="form-control2" placeholder="example - Toyota" value="">
												<br>
												<h5>Milage</h5>
                                                <input id="vehicleMilage" type="text" class="form-control2" placeholder="123KM" value="">
                                                   <br>
                                                    <div class="dropdown">
                                                        <h5>Status</h5>&nbsp &nbsp &nbsp
                                                        <select id="status" class="btn btn-default dropdown-toggle" >
                                                            <option value="ACT">Active</option>
                                                            <option value="INA">Inactive</option>
                                                        </select>

                                                    </div>
												<br>
                                                    <br>
													<button type="submit" id="vehicleSave" class="btn btn-info btn-fill pull-left">Save</button>
                                                    <button type="submit" id="vehicleUpdate" class="btn btn-info btn-fill pull-left">Update</button>
													<br>
													<br>
													<br>
													
                                    

												
												</div>
												<br> <br>
												
												
											  

												
                                            </div>
                                        </div>
									</div>

									
								
                                </form>
							</div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                    <div class="pull-center">
                                        <h5>Search Vehicle</h5>

                                        <input type="text" id="serchVehicleRegNo" class="form-control1" placeholder="TYPE YOUR REG NO HERE" name="search"><br>
                                        <button type="submit" onclick="searchVehicle()" class="btn btn-info btn-fill pull-center">Search</button>&nbsp &nbsp &nbsp

                                        <br>
                                    </div><br><br>

                                    <div class="content table-responsive table-full-width">
                                        <table id="vehicleTable" class="display" width="100%"></table>

                                        <br>
                                        <br>
                                        <div class="pull-center">
                                            <button type="submit" id="vehicleAddButton" onclick="vehicleAdd()" class="btn btn-info btn-fill pull-center">Add</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleUpdateButton" onclick="vehicleUpdate()" class="btn btn-info btn-fill pull-center">Edit</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleDeleteButton" onclick="vehicleDelete()" class="btn btn-info btn-fill pull-center">Delete</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleClearButton" onclick="vehicleClear()" class="btn btn-info btn-fill pull-center">Clear</button>
                                            <br>
                                        </div>

                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
                </div>
            </div>
        </div>                  
                                    
                 
                    

                


        <footer class="footer">

        </footer>

    </div>
</div>

									


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <script src="assets/js/custom/manageVehicle.js"></script>
	
	

</html>
