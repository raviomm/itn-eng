<html lang="en"><head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">

    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script></head>
<body style="">
<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Sithumina Transport
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li>
                <a href="user-accout.html">
                    <i class="pe-7s-user"></i>
                    <p>User Accounts</p>
                </a>
            </li>
            <li>
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li></li><li class="">
            <a href="manageVehicle.html">
                <i class="pe-7s-note2"></i>
                <p>Vehicle</p>
            </a>
        </li>
            <li class="active">
                <a href="manageRoute.html">
                    <i class="pe-7s-news-paper"></i>
                    <p>Route</p>
                </a>
            </li>
            <li>
                <a href="manageProduct.html">
                    <i class="pe-7s-science"></i>
                    <p>Product</p>
                </a>
            </li>
            <li>
                <a href="manageFleet.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Fleets</p>
                </a>
            </li>
            <li>
                <a href="manageFleetHistory.html">
                    <i class="pe-7s-graph"></i>
                    <p>Fleet History</p>
                </a>
            </li>
            <li>
                <a href="table.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="table.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
            <li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div></div>

<div class="wrapper">
    <div class="main-panel">

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Route Managing Page</h4>
                            </div>

                            <div class="content">
                                <form id="routeForm">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="pull-left">
                                                    <input id="routeId" type="text" hidden>
                                                    <h5>Route Name</h5>
                                                    <input id="routeName" type="text" class="form-control2" placeholder="Enter user name" value="">
                                                    <br>
                                                    <div class="dropdown">
                                                        <h5>Origin</h5>
                                                        <select id="routeOrigin" class="btn btn-default dropdown-toggle" >
                                                            <option value=""></option>
                                                            <option value="Ranala">Ranala</option>
                                                            <option value="Rathmalana">Rathmalana</option>
                                                            <option value="Kurunegala">Kurunegala</option>
                                                            <option value="Kottawa">Kottawa</option>
                                                            <option value="Matara">Matara</option>
                                                            <option value="Kandy">Kandy</option>
                                                            <option value="Anuradhapura">Anuradhapura</option>
                                                        </select>
                                                    </div>

                                                    <div class="dropdown">
                                                        <h5>Destination</h5>
                                                        <select id="routeDestination" class="btn btn-default dropdown-toggle" >
                                                            <option value=""></option>
                                                            <option value="Ranala">Ranala</option>
                                                            <option value="Rathmalana">Rathmalana</option>
                                                            <option value="Kurunegala">Kurunegala</option>
                                                            <option value="Kottawa">Kottawa</option>
                                                            <option value="Matara">Matara</option>
                                                            <option value="Kandy">Kandy</option>
                                                            <option value="Anuradhapura">Anuradhapura</option>
                                                        </select>
                                                    </div>
                                                    <br>
                                                    <h5>Distance</h5>
                                                    <input id="routeDistance" type="text" class="form-control2" placeholder="Enter Distance" value="">
                                                    <br>
                                                    <h5>Duration</h5>
                                                    <input id="routeDuration" type="text" class="form-control2" placeholder="Enter Duration" value="">
                                                    <br>
                                                    <h5>Status</h5>
                                                    <div class="dropdown">
                                                        <select id="routeStatus" class="btn btn-default dropdown-toggle" >
                                                            <option value="ACT">Active</option>
                                                            <option value="INA">Inactive</option>
                                                        </select>
                                                    </div>
                                                    <br>
                                                    <button type="submit" id="routeSaveButton" class="btn btn-info btn-fill pull-left">Save</button>
                                                    <button type="submit" id="routeUpdateButton" class="btn btn-info btn-fill pull-left">Update</button>
                                                    <br>
                                                    <br>


                                                </div>
                                                <br> <br>


                                            </div>
                                        </div>
                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>




                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                <div class="pull-center">
                                    <h5>Search Route</h5>
                                    <input type="text" id="searchRouteId" class="form-control1" placeholder="TYPE YOUR ROUTE ID HERE" name="search"><br>
                                    <button type="submit" onclick="searchRoute()" class="btn btn-info btn-fill pull-center">Search</button>&nbsp &nbsp &nbsp


                                    <br>
                                </div><br><br>

                                <div class="content table-responsive table-full-width">
                                    <table id="routeTable" class="display" width="100%"></table>

                                    <br>
                                    <br>
                                    <div class="pull-center">
                                        <button type="submit" id="routeAddButton" onclick="routeAdd()" class="btn btn-info btn-fill pull-center">Add</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeEditButton" onclick="routeEdit()" class="btn btn-info btn-fill pull-center">Edit</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeDeleteButton" onclick="routeDelete()" class="btn btn-info btn-fill pull-center">Delete</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeClearButton" onclick="routeClear()" class="btn btn-info btn-fill pull-center">Clear</button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <footer class="footer">

            </footer>

        </div>
    </div>






    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <!-- JQuery data table adding -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <!-- Include the js file to the page -->
    <script src="assets/js/custom/manageRoute.js"></script>



</body></html>