<%--
  Created by IntelliJ IDEA.
  User: RAVI KALUARACHCHI
  Date: 2/18/2019
  Time: 1:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">

    <!--  time picker needs     -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">

    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>


    <%--<!--     for datatable select     -->
    <script src='https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js'></script>--%>



</head>
<body style="">
<div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                ITN ENG
            </a>
        </div>

        <ul class="nav">

            <li>
                <a href="manageBf.html">
                    <i class="pe-7s-note2"></i>
                    <p>Booking Form</p>
                </a>
            </li>
            <li class="active">
                <a href="manageReservationEdit.html">
                    <i class="pe-7s-note2"></i>
                    <p>Reservation Edit</p>
                </a>
            </li>
            <li >
                <a href="manageProgrammeDgmA.html">
                    <i class="pe-7s-note2"></i>
                    <p>Programme Manager Page</p>
                </a>
            </li>
            <li >
                <a href="manageProgrammeDgmAtwo.html">
                    <i class="pe-7s-note2"></i>
                    <p>Programme DGM Page</p>
                </a>
            </li>
            <%--<sec:authorize access="hasAuthority('ADMIN') ">--%>
                <li >
                    <a href="manageFinalEngReleasePage.html">
                        <i class="pe-7s-note2"></i>
                        <p>Eng Items Release</p>
                    </a>
                </li>
           <%-- </sec:authorize>--%>
            <li >
                <a href="manageReports.html">
                    <i class="pe-7s-note2"></i>
                    <p>Reports</p>
                </a>
            </li>



            <%--<li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>--%>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div>
</div>

<div class="wrapper">
    <div class="main-panel">


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Reservation status and Edit Page</h4>
                            </div>
                            <div class="content">
                                <div class="row">

                                    <div class="content">
                                        <form id="reservationEdit"  data-toggle="validator" role="form">

                                            <div class="btn-toolbar">
                                           <%-- <button id="itemsEditButton" onclick="itemsEdit()" class="btn btn-success btn-fill pull-left"  type="button">Edit
                                            </button>--%>
                                            <button id="itemsResetButton" onclick="itemsReset()" class="btn btn-success btn-fill pull-left"  type="button">Reset
                                            </button>
                                            </div>
                                            <br>


                                            <div class="content table-responsive table-full-width hover">
                                                <table id="reservationEditTable" class="display" width="100%"></table>

                                                <br>
                                                <br>
                                                <%--<button type="submit" id="bookformSave"
                                                        class="btn btn-info btn-fill pull-left">Save
                                                </button>--%>
                                                <br>
                                                <br><br>
                                                <br>

                                            </div>






                                            <br> <br>



                                        </form>
                                    </div>

                                </div>
                                <div class="clearfix"></div>



                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="content">
                                <div class="row">
                                    <div class="content">
                                        <form id="reservationEdit1"  data-toggle="validator" role="form">
                                            <div class="form-group">
                                                <label for="selectType"
                                                       class="control-label required"><b>Type</b></label>&nbsp &nbsp &nbsp&nbsp &nbsp &nbsp&nbsp &nbsp
                                                <select id="selectType" onChange="getSelectSubtype()"
                                                        data-error="Select a Type"
                                                        class=" btn btn-default dropdown-toggle" required>
                                                    <option value="">Select a Type</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <br>

                                            <div class="form-group">
                                                <label for="selectSubtype"
                                                       class="control-label required"><b>Sub Type</b></label>&nbsp &nbsp &nbsp
                                                <select id="selectSubtype" onChange="getSelectItemCode()"
                                                        data-error="Select a Sub Type"
                                                        class=" btn btn-default dropdown-toggle" required>
                                                    <option value="">Select a Sub Type</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <br>

                                            <div class="form-group">
                                                <label for="itemCode"
                                                       class="control-label required"><b>Item Code</b></label> &nbsp&nbsp
                                                <select id="itemCode"
                                                        data-error="Select a Item Code"
                                                        class=" btn btn-default dropdown-toggle" required>
                                                    <option value="">Select a Item Code</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <br>
                                            <br>
                                            <button id="inventoryAdd" class="btn btn-info btn-fill pull-left" disabled type="button">Add Item
                                            </button>
                                            <br>
                                            <br><br>


                                            <div  class="content table-responsive table-full-width hover " >
                                                <table id="reservationEditItemTable" class="display " width="100%"></table>

                                            </div>
                                            <br>
                                            <br>
                                            <div class="btn-toolbar">
                                            <button id="editSubmit" class="btn btn-success btn-fill pull-left"  type="button">Submit
                                            </button>
                                            </div>
                                            <br><br>
                                            <div class="form-group">
                                                <label for="textareaComment"><b>Rejected Fact</b></label><br>
                                                <textarea rows="5" cols="20" id="textareaComment" placeholder="Click comment button to show the comment"></textarea>
                                            </div>
                                            <br> <br>
                                            <%--<div class="btn-toolbar">
                                                <button id="rejectedCommentButton" onclick="rejectedComment()" class="btn btn-warning btn-fill pull-left"  type="button"> Comment
                                                </button>
                                                <br>
                                                <br>

                                                <br>
                                            </div>--%>

                                        </form>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <footer class="footer">

        </footer>

    </div>
</div>

<!-- Alert Box -->
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="assets/js/sweetalert2.min.js"></script>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/validator.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<!-- JQuery data table adding -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<!-- Include the js file to the page -->
<script src="assets/js/custom/manageReservationEdit.js"></script>


</div>
</body>
</html>