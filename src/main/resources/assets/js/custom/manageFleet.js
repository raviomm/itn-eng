/**
 * Created by RAVI KALUARACHCHI on 12/12/2017.
 */
/**
 *Created by RAVI KALUARACHCHI on 12/8/2017.
 */

var clickedRow = false;
var table;

$(document).ready(function() {

    table = $('#fleetTable').DataTable({
        processing: true,
        ajax: {
            url: "/fleet/getAllFleets",
            "type": "POST",
            "data": function() {
                var fleetSearchCriteria = {};
                fleetSearchCriteria.fleetName = $("#searchFleetName").val();
                return fleetSearchCriteria;
            },
            dataSrc: function(json) {

                return json.tableData;
            }
        },
        columns: [{
            title: "fleet Id",
            data: "fleetId"
        },
            {
                title: "Fleet Name",
                data: "fleetName"
            },
            {
                title: "Route Used",
                data: "selectRoute"
            },
            {
                title: "Product Used",
                data: "selectProduct"
            },
            {
                title: "Vehicle Used",
                data: "selectVehicle"
            },
            {
                title: "Driver Assigned",
                data: "selectDriver"
            },
            {
                title: "Helper Assigned",
                data: "selectHelper"
            },
            {
                title: "Start Time",
                data: "fleetStartTime"
            },
            {
                title: "Petty Cash",
                data: "fleetPettyCash"
            },
            {
                title: "Petty Description",
                data: "fleetPettyCashDescription"
            },
            {
                title: "Due Date",
                data: "fleetDueDate"
            },
            {
                title: "Fleet Status",
                data: "fleetStatus"
            }
        ],
        columnDefs: [{
            targets: [2],
            visible: false,
            searchable: false
        },
            {
                targets: [8],
                visible: false,
                searchable: false
            },
            {
                targets: [9],
                visible: false,
                searchable: false
            }
        ]



    });


    // table click event
    $('#fleetTable tbody').on('click', 'tr', function() {
        var data = table.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    });


    var selectRoute = $("#selectRoute");

    $.ajax({
        url: '/route/getAllRoutes', // or whatever
        dataType: 'json',
        type: 'post',
        success: function(response) {
            $.each(response.tableData, function() {
                selectRoute.append($("<option />").val(this.routeId).text(this.routeName));
            });

        }
    });

    var selectProduct = $("#selectProduct");

    $.ajax({
        url: '/product/getAllProducts', // or whatever
        dataType: 'json',
        type: 'post',
        success: function(response) {
            $.each(response.tableData, function() {
                selectProduct.append($("<option />").val(this.productId).text(this.productName));
            });

        }
    });

    var selectVehicle = $("#selectVehicle");

    $.ajax({
        url: "/vehicle/getAllVehicles", // or whatever
        dataType: 'json',
        type: 'post',
        success: function(response) {
            $.each(response.tableData, function() {
                selectVehicle.append($("<option />").val(this.id).text(this.vehcleRegNo));
            });

        }
    });

    var selectDriver = $("#selectDriver");

    $.ajax({
        url: "/fleet/getAllDrivers",
        dataType: 'json',
        type: 'post',
        success: function(response) {
            $.each(response.tableData, function() {
                selectDriver.append($("<option />").val(this.employeeId).text(this.employeeNIC));
            });

        }
    });


    var selectHelper = $("#selectHelper");

    $.ajax({
        url: "/fleet/getAllHelpers",
        dataType: 'json',
        type: 'post',
        success: function(response) {
            $.each(response.tableData, function() {
                selectHelper.append($("<option />").val(this.employeeId).text(this.employeeNIC));
            });
        }
    });




    $( function() {
        $( "#fleetDueDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
    } );


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#fleetUpdateButton").hide();
    $("#fleetSaveButton").show();

});


function fleetAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $("#fleetSaveButton").show();
    $("#fleetUpdateButton").hide();

}

function resetForm() {
    $(':input', '#fleetForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#fleetName").prop('disabled', disable);
    $("#selectRoute").prop('disabled', disable);
    $("#selectProduct").prop('disabled', disable);
    $("#selectVehicle").prop('disabled', disable);
    $("#selectDriver").prop('disabled', disable);
    $("#selectHelper").prop('disabled', disable);
    $("#fleetStartTime").prop('disabled', disable);
    $("#fleetPettyCash").prop('disabled', disable);
    $("#fleetPettyCashDescription").prop('disabled', disable);
    $("#fleetDueDate").prop('disabled', disable);
    $("#fleetStatus").prop('disabled', disable);

}

function fillFormData(row) {
    $("#fleetId").val(row.fleetId);
    $("#fleetName").val(row.fleetName);
    $("#selectRoute").val(row.selectRoute);
    $("#selectProduct").val(row.selectProduct);
    $("#selectVehicle").val(row.selectVehicle);
    $("#selectDriver").val(row.selectDriver);
    $("#selectHelper").val(row.selectHelper);
    $("#fleetStartTime").val(row.fleetStartTime);
    $("#fleetPettyCash").val(row.fleetPettyCash);
    $("#fleetPettyCashDescription").val(row.fleetPettyCashDescription);
    $("#fleetDueDate").val(row.fleetDueDate);
    $("#fleetStatus").val(row.fleetStatus);

}


function disableUpdate(disable) {
    $("#fleetEditButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#fleetDeleteButton").prop('disabled', disable);

}

function fleetClear(disable) {
    $("#fleetClearButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $("#fleetUpdateButton").hide();
    $("#fleetSaveButton").show();

}


function fleetSave() {

    var fleetDTO = {};
    fleetDTO.fleetId = $("#fleetId").val();
    fleetDTO.fleetName = $("#fleetName").val();
    fleetDTO.selectRoute = $("#selectRoute").val();
    fleetDTO.selectProduct = $("#selectProduct").val();
    fleetDTO.selectVehicle = $("#selectVehicle").val();
    fleetDTO.selectDriver = $("#selectDriver").val();
    fleetDTO.selectHelper = $("#selectHelper").val();
    fleetDTO.fleetStartTime = $("#fleetStartTime").val();
    fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
    fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
    fleetDTO.fleetDueDate = $("#fleetDueDate").val();
    fleetDTO.fleetStatus = $("#fleetStatus").val();


    $.ajax({
        url: "/fleet/saveFleetRecord", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Saved the Fleet ");
                fleetClear(true);
            } else {
                alert("Error Saving Fleet  !!!");
            }
        },
        error : function (error) {
            console.log(error);
        }
    });

}

function fleetUpdate() {

    var fleetDTO = {};
    fleetDTO.fleetId = $("#fleetId").val();
    fleetDTO.fleetName = $("#fleetName").val();
    fleetDTO.selectRoute = $("#selectRoute").val();
    fleetDTO.selectProduct = $("#selectProduct").val();
    fleetDTO.selectVehicle = $("#selectVehicle").val();
    fleetDTO.selectDriver = $("#selectDriver").val();
    fleetDTO.selectHelper = $("#selectHelper").val();
    fleetDTO.fleetStartTime = $("#fleetStartTime").val();
    fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
    fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
    fleetDTO.fleetDueDate = $("#fleetDueDate").val();
    fleetDTO.fleetStatus = $("#fleetStatus").val();


    $.ajax({
        url: "/fleet/updateFleet", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Update the Fleet ");
                fleetClear(true);
            } else {
                alert("Error Updating Fleet  !!!");
            }
        },
        error : function (error) {
            console.log(error);
        }
    });

}

function fleetEdit(){
    if(clickedRow){
        disableFormData(true);
        $("#fleetName").prop('disabled', false);
        $("#selectProduct").prop('disabled', false);
        $("#selectRoute").prop('disabled', false);
        $( "#fleetUpdateButton" ).show();
        $( "#fleetSaveButton" ).hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}

function fleetDelete() {
    if (clickedRow) {
        if (confirm("Are you sure to delete the selected fleet") == true) {
            var fleetDTO = {};
            fleetDTO.fleetId = $("#fleetId").val();
            fleetDTO.fleetName = $("#fleetName").val();
            fleetDTO.selectRoute = $("#selectRoute").val();
            fleetDTO.selectProduct = $("#selectProduct").val();
            fleetDTO.selectVehicle = $("#selectVehicle").val();
            fleetDTO.selectDriver = $("#selectDriver").val();
            fleetDTO.selectHelper = $("#selectHelper").val();
            fleetDTO.fleetStartTime = $("#fleetStartTime").val();
            fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
            fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
            fleetDTO.fleetDueDate = $("#fleetDueDate").val();
            fleetDTO.fleetStatus = $("#fleetStatus").val();

            $.ajax({
                url: '/fleet/deleteFleet', // or whatever
                dataType: 'json',
                type: 'post',
                data: fleetDTO,
                success: function(response) {
                    if (response.success) {
                        alert("Successfully deleted the Fleet ");
                        fleetClear(true);
                    } else {
                        alert("Error while deleting Fleet  !!!");
                    }
                }
            });
        }

    } else {
        alert("Please Select a Row first !!!!");
    }
}

function searchFleet() {
    table.ajax.reload();
}