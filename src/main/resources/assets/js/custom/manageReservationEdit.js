/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
var originalInvMap = [];
var addedInvMap = [];
var inventoryObj = {};
var table;
var table1;
var editor;
var nowBookingId;

$(document).ready(function () {


    var table = $('#reservationEditTable').DataTable( {
        processing: true,
        ajax: {
            url: "/reservationEdit/getAllEngs",
            "type": "GET",
            dataSrc: function (json) {

                console.log(json.tableData);
                if(json.tableData.length > 0 ){
                    json.tableData.forEach(function(row) {
                        console.log(row);
                        var sDate = row.startDate;
                        row.startDate = new Date(sDate).toString().substr(0,15);
                        var eDate = row.endDate;
                        row.endDate = new Date(eDate).toString().substr(0,15);
                        var rDate = row.reservationRequestDate;
                        row.reservationRequestDate = new Date(rDate).toString().substr(0,15);
                    });
                    return json.tableData;
                }
                return [];
            }
        },



        columns: [

            {title: "Booking ID", data: 'bookingId'},
            {title: "Request Date", data: 'reservationRequestDate'},
            {title: "Start Date", data: 'startDate'},
            {title: "End Date", data: 'endDate'},
            {title: "Location", data: 'eventLocation'},
            {title: "Programme Title", data: 'titleOfProgramme'},
            {title: "Status", data: 'bookingStatus'},
            {title: "Edit"}
        ],
        columnDefs: [

          {

                targets: [7],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.bookingId +'" class="btn btn-info btn-fill btn-sm" type="button" onclick="itemsEdit(this)" name="editButtons">Edit</button>'
                }

            }
        ],

        order: [[1, 'asc']]
    } );



    var selectType = $("#selectType");
    $.ajax({
        url: "/bookingForm/getAllTypes",
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                selectType.append($("<option />").val(this).text(this));
            });

        }
    });

//inventoryAdd Button Things
    $("#inventoryAdd").click(function () {

        /*document.getElementById("editSubmit").disabled = false;*/

        disableSubmitButton(false);

        const itemCode = $('#itemCode :selected').text();
        const subType = $('#selectSubtype :selected').text();
        const type = $('#selectType :selected').text();


        if (itemCode !== undefined) {
            const index = addedInvMap.map(
                function (object,index) {

                    return object.itemCode;

                }).indexOf(itemCode);

            if (index >= 0) {
                alert("Can't add same item code twice");
                document.getElementById("inventoryAdd").disabled = true;
            }
            else {
                const updatedItem = {itemCode: itemCode,subType: subType, type: type,serial:itemCode};
                addedInvMap.push(updatedItem);
                table1.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

    /*document.getElementById("editSubmit").disabled = true;*/
    disableSubmitButton(true);


    //use to enable disable button
    $('#itemCode').on('change', function() {
        if(this.value){
            document.getElementById("inventoryAdd").disabled = false;
            return;
        }
        document.getElementById("inventoryAdd").disabled = true;
    });

    disableTextareaComment(true);
    disableCommentButton(true);


});



function tableItemEdit(item) {
    var type = $(item).closest('tr').find('td:nth-child(1)').html();
    var subType = $(item).closest('tr').find('td:nth-child(2)').html();
    var code = $(item).closest('tr').find('td:nth-child(3)').html();
    $("#selectType").val(type);
    getSelectSubtype(subType);
    getSelectItemCode(subType, code);

    const index = addedInvMap.map(
        function (object, index) {

            return object.itemCode;

        }).indexOf(code);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table1
        .row( $(item).parents('tr') )
        .remove()
        .draw();
}

function getSelectSubtype(subType) {
    var selectedType = document.getElementById("selectType").value;
    var selectSubtype = $("#selectSubtype");
    selectSubtype.empty();
    selectSubtype.append($("<option />").val(''));
    $.ajax({
        url: "/bookingForm/getSubType?type=" + selectedType,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                //selectType.append($("<option />").val(this).text(this));
                selectSubtype.append($("<option />").val(this).text(this));
            });
            if(subType) $("#selectSubtype").val(subType);

        }
    });
}

//get the item code according to the type and sub type
function getSelectItemCode(subType,code) {

    /*var selectedType = document.getElementById("selectType").value;*/
    var selectedSubType = document.getElementById("selectSubtype").value;
    if(subType) selectedSubType = subType;
    var selectItemCode = $("#itemCode");
    selectItemCode.empty();
    selectItemCode.append($("<option />").val(''));
    $.ajax({
        url: "/bookingForm/getItemCode?subType=" + selectedSubType,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                //selectType.append($("<option />").val(this).text(this));
                selectItemCode.append($("<option />").val(this).text(this));
            });
            if(code) $("#itemCode").val(code);
        }
    });
}






function itemsReset() {

    window.location.reload();
}


function itemsEdit(obj) {

    disableAllEditButtons(true);
    disableCommentButton(false);


    var bookingId = $(obj).closest('tr').find('td:first').html();
    table1 = $('#reservationEditItemTable').DataTable({
        processing: true,
        ajax: {
            url: "/reservationEdit/getAllEngItems?bookingId=" + bookingId,
            dataType: 'json',
            type: 'get',
            dataSrc: function (json) {
                return json.tableData;
            }
        },
        dom: 'Bfrtip',
        columns: [
            {title: "Type", data: 'type'},
            {title: "Sub Type", data: 'subType'},
            {title: "Serial", data: 'serial'}
        ],
        columnDefs: [

            {

                targets: [3],
                data: null,
                render: function (data, type, row) {
                    return '<button id="' + row.inventoryId + '" class="btn btn-warning btn-fill btn-sm " type="button"onclick="tableItemEdit(this)">Remove</button>'
                }

            }
        ],
        order: [[1, 'asc']],
        destroy: ['true']

    });


    $("#editSubmit").click(function (e) {


        var BookingFormDTO = {};
        var data = table1.rows().data();
        console.log(table1.rows().data());
        console.log(addedInvMap);
        delete data.context;
        //delete data.length;   // Do not delete this one! Needed for the loop below.

        delete data.selector;
        delete data.ajax;


        var dataAsArray = [];

        for (i = 0; i < data.length; i++) {
            dataAsArray.push(data[i]);
        }

        BookingFormDTO.items = dataAsArray;
        BookingFormDTO.bookingId = bookingId;
        e.preventDefault();
        $.ajax({
            url: '/reservationEdit/updateBookingForm', // or whatever
            dataType: 'json',
            contentType: "application/json",
            type: 'post',
            data: JSON.stringify(BookingFormDTO),
            success: function (response) {
                if (response.success) {
                    alert("Successfully Updated the Booking ");
                } else {
                    alert("Error Updating Booking  !!!");
                }
            }
        });
        return false;

    })


    $("#rejectedCommentButton").click(function () {


        $.ajax({
            url: '/reservationEdit/getCommentbyBookingId?bookingId=' + bookingId, // or whatever
            dataType: 'text',
            type: 'get',
            success: function (response) {
                console.log(response);
                /*textareaCommet = response.comment.prop;*/
                $("#textareaComment").text(response);

            }
        });
    })





}



function disableAllEditButtons(disable) {


    $('.btn-info').attr('disabled', disable);

}


function disableSubmitButton(disable) {

    if (disable == true){
        document.getElementById("editSubmit").disabled = true;
    }
    else{
        document.getElementById("editSubmit").disabled = false;
    }

}


function disableTextareaComment(disable) {

    if (disable == true){
        document.getElementById("textareaComment").disabled = true;
    }
    else{
        document.getElementById("textareaComment").disabled = false;
    }

}

function disableCommentButton(disable) {

    if (disable == true){
        document.getElementById("rejectedCommentButton").disabled = true;
    }
    else{
        document.getElementById("rejectedCommentButton").disabled = false;
    }

}
