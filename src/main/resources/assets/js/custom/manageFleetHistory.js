/**
 * Created by RAVI KALUARACHCHI on 6/11/2018.
 */
var table;
/**
 * Created by RAVI KALUARACHCHI on 11/06/2018.
 */
var clickedRow = false;

$(document).ready(function() {

    table = $('#fleetHistoryTable').DataTable({
        processing: true,
        ajax: {
            url: "/fleetHistory/getAllFleetHistoryTable",
            "type": "POST",
            "data": function() {
                var fleetSearchCriteria = {};
                fleetSearchCriteria.fleetName = $("#searchFleetName").val();
                return fleetSearchCriteria;

            },
            dataSrc: function(json) {

                return json.tableData;
            }
        },
        columns: [
            {
                title: "fleet Id",
                data: "fleetId"
            },
            {
                title: "Fleet Name",
                data: "fleetName"
            },
            {
                title: "Route Used",
                data: "selectRoute"
            },
            {
                title: "Product Used",
                data: "selectProduct"
            },
            {
                title: "Vehicle Used",
                data: "selectVehicle"
            },
            {
                title: "Driver Assigned",
                data: "selectDriver"
            },
            {
                title: "Helper Assigned",
                data: "selectHelper"
            },
            {
                title: "Start Time",
                data: "fleetStartTime"
            },
            {
                title: "Petty Cash",
                data: "fleetPettyCash"
            },
            {
                title: "Petty Description",
                data: "fleetPettyCashDescription"
            },
            {
                title: "Due Date",
                data: "fleetDueDate"
            },
            {
                title: "Fleet Status",
                data: "fleetStatus"

            }

        ],
        columnDefs: [{
            targets: [2],
            visible: false,
            searchable: false
        },
            {
                targets: [8],
                visible: false,
                searchable: false
            },
            {
                targets: [11],
                visible: true,
                searchable: false
            }

        ],

        //Table Row Color code
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            switch(aData.fleetStatus){
                case 'ACT':
                    $(nRow).css('color', 'green');
                    break;
                case 'INA':
                    $(nRow).css('color', 'red');
                    break;

            }
        }

    });
// New code added for  button




    // New code added for  button ends here


    // table click event
    $('#homeTable tbody').on('click', 'tr', function() {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }


    });




});


