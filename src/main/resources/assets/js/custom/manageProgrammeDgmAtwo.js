/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
var originalInvMap = [];
var addedInvMap = [];
var inventoryObj = {};
var table;
var table1;

$(document).ready(function () {


    var table = $('#engProgrammeApprovaltwo').DataTable( {

        processing: true,
        ajax: {
            url: "/engPApprovaltwo/getAllEngstwo",
            "type": "GET",
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            /*{
             className:      'details-control',
             orderable:      false,
             data:           null,
             defaultContent: ''
             },*/
            {title: "Booking ID", data: 'bookingId'},
            {title: "Start Date", data: 'startDate'},
            {title: "End Date", data: 'endDate'},
            {title: "Location", data: 'eventLocation'},
            {title: "Programme Title", data: 'titleOfProgramme'},
            {title: "Status", data: 'bookingStatus'},
            {title: "Select" },
            {title: "Approval" }
        ],
        columnDefs: [

            {

                targets: [6],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" type="button" onclick="selectButtonPmtwo(this)">Select</button>'
                }

            }
            ,{

                targets: [7],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" onclick="statusApprovePmtwo(this)">Approve</button>&nbsp<button id="' + row.id +'" onclick="statusCancelPmtwo(this)">Cancel</button>'
                }

            }
        ],
        order: [[1, 'asc']]
    } );





});




function selectButtonPmtwo(obj) {

    var bookingId = $(obj).closest('tr').find('td:first').html();

    table1 = $('#engProgrammeApprovalItemstwo').DataTable({

        processing: true,

        ajax: {
            url: "/engPApprovaltwo/getAllEngItemstwo?bookingId="+bookingId,
            dataType: 'json',
            type: 'get',
            dataSrc: function (json) {


                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [

            {title: "Inventory ID", data: 'inventoryId'},
            {title: "Type", data: 'type'},
            {title: "Sub Type", data: 'subType'},
            {title: "Serial", data: 'serial'}
        ],

        order: [[1, 'asc']],
        destroy: ['true']


    } );

    return false;

}


// Approve Button function

function statusApprovePmtwo(obj,bookingId) {

    var bookingformDTO = {};
    bookingId = $(obj).closest('tr').find('td:first').html();
    bookingformDTO.bookingId = bookingId;

    $.ajax({
        url: "/engPApprovaltwo/statusApprovePmtwo", // or whatever
        dataType: 'json',
        type: 'post',
        data:bookingformDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Approved by Programme DGM ");
            } else {
                alert("Error Approving Reservation  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });

}




//Cancel Button Function

function statusCancelPmtwo(obj,bookingId) {

    var bookingformDTO = {};
    bookingId = $(obj).closest('tr').find('td:first').html();
    bookingformDTO.bookingId = bookingId;


    $.ajax({
        url: "/engPApprovaltwo/statusCancelPmtwo", // or whatever
        dataType: 'json',
        type: 'post',
        data:bookingformDTO,
        success: function(response) {
            if (response.success) {
                alert("ENG Booking Canceled ");
            } else {
                alert("Error Cancelling Reservation  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });

}
