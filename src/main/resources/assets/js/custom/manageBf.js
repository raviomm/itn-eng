/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
var originalInvMap = [];
var addedInvMap = [];
var inventoryObj = {};
var table;



$(document).ready(function () {
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('bookingId');
    console.log(myParam);




    table = $('#inventoryTable').DataTable({
        columns: [
            {title: "Type", data: 'type'},
            {title: "Sub Type", data: 'subType'},
            {title: "Item Serial", data: 'itemCode'},
            {title: "Remove"}


        ],
        columnDefs: [

           {

                targets: [3],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="itemsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.inventoryId);
        }
    });


    //Type Load

    var selectType = $("#selectType");

    $.ajax({
        url: "/bookingForm/getAllTypes",
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                selectType.append($("<option />").val(this).text(this));
            });

        }
    });




    $("#inventoryAdd").click(function () {
        const itemCode = $('#itemCode :selected').text();
        const subType = $('#selectSubtype :selected').text();
        const type = $('#selectType :selected').text();


        if (itemCode !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.itemCode;

                }).indexOf(itemCode);

            if (index >= 0) {
                alert("Can't add same item code twice");
                document.getElementById("inventoryAdd").disabled = true;
            }
            else {
                const updatedItem = {itemCode: itemCode,subType: subType, type: type };
                addedInvMap.push(updatedItem);
                table.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });



    $('#itemCode').on('change', function() {
        if(this.value){
            document.getElementById("inventoryAdd").disabled = false;
            return;
        }
        document.getElementById("inventoryAdd").disabled = true;
    });




    $('#inventoryTable tbody').on( 'click', 'button.btn-warning', function () {
        var code = $(this).closest('tr').find('td:nth-child(3)').html();
        const index = addedInvMap.map(
            function (object, index) {

                return object.itemCode;

            }).indexOf(code);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

});


//function for Edit button on table column
function itemsRemove(object) {
    var type = $(object).closest('tr').find('td:nth-child(1)').html();
    var subType = $(object).closest('tr').find('td:nth-child(2)').html();
    var code = $(object).closest('tr').find('td:nth-child(3)').html();
    $("#selectType").val(type);
    getSelectSubtype(subType);
    getSelectItemCode(subType, code);

    const index = addedInvMap.map(
        function (object, index) {

            return object.itemCode;

        }).indexOf(code);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}



//get the subtype list according to the selected type
function getSelectSubtype(subType) {
    var selectedType = document.getElementById("selectType").value;
    console.log(selectedType);
    var selectSubtype = $("#selectSubtype");
    selectSubtype.empty();
    selectSubtype.append($("<option />").val(''));
    $.ajax({
        url: "/bookingForm/getSubType?type=" + selectedType,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                //selectType.append($("<option />").val(this).text(this));
                selectSubtype.append($("<option />").val(this).text(this));
            });
            if(subType) $("#selectSubtype").val(subType);

        }
    });
}




//get the item code according to the type and sub type
function getSelectItemCode(subType,code) {

    /*var selectedType = document.getElementById("selectType").value;*/
    var selectedSubType = document.getElementById("selectSubtype").value;
    if(subType) selectedSubType = subType;
    var selectItemCode = $("#itemCode");
    selectItemCode.empty();
    selectItemCode.append($("<option />").val(''));
    $.ajax({
        url: "/bookingForm/getItemCode?subType=" + selectedSubType,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $.each(response, function () {
                //selectType.append($("<option />").val(this).text(this));
                selectItemCode.append($("<option />").val(this).text(this));
            });
            if(code) $("#itemCode").val(code);
        }
    });
}






//save booking data

$("#bookformSave").click(function () {

    var BookingFormDTO = {};
    var data = table.rows().data();
    console.log(table.rows().data());
    console.log(addedInvMap);
    delete data.context;
    //delete data.length;   // Do not delete this one! Needed for the loop below.
    delete data.selector;
    delete data.ajax;

    var dataAsArray = [];

    for(i=0;i<data.length;i++){
        dataAsArray.push(data[i]);
    }

    BookingFormDTO.bookingId = $("#bookingId").val();
    BookingFormDTO.reservationRequestDate = $("#reservationRequestDate").val();// this field to record time and date
    BookingFormDTO.startDate = $("#startDate").val();
    BookingFormDTO.endDate = $("#endDate").val();
    BookingFormDTO.eventLocation = $("#eventLocation").val();
    BookingFormDTO.titleOfProgramme = $("#titleOfProgramme").val();
    BookingFormDTO.bookingStatus = $("#bookingStatus").val();
    BookingFormDTO.items = dataAsArray;


    $.ajax({
        url: '/bookingForm/saveBookingForm', // or whatever
        dataType: 'json',
        contentType: "application/json",
        type: 'post',
        data: JSON.stringify(BookingFormDTO),
        success: function (response) {
            if (response.success) {
                alert("Successfully Saved the Booking ");
                bookingClear(true);
            } else {
                alert("Error Saving Booking  !!!");
            }
        }
    });
    return false;
});







$('#itemCode').on('change', function() {
    if(this.value){
        document.getElementById("bookformSave").disabled = false;
        return;
    }
    document.getElementById("bookformSave").disabled = true;
});











