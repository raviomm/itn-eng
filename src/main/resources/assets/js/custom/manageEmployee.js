$(document).ready(function(){

    table = $('#employeeTable').DataTable( {
        processing : true,
        ajax: {
            url: "/employee/getAllEmployees",
            "type" : "POST",
            "data":function () {
                var employeeSearchCriteria = {};
                employeeSearchCriteria.employeeNIC = $( "#serchEmployeeNIC" ).val();
                return employeeSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            { title: "Employee Id" , data: "employeeId"},
            { title: "First Name", data: "employeeFirstName"},
            { title: "Last Name",data: "employeeLastName" },
            { title: "NIC", data: "employeeNIC" },
            { title: "Employee EPF No", data: "employeeEPF" },
            { title: "Address", data: "employeeAddress" },
            { title: "Mobile Number", data: "employeeMobileNumber"},
            { title: "Email", data: "employeeEmail"},
            { title: "Basic Salary", data: "employeeBasicSalary"},
            { title: "Allowance", data: "employeeAllowance"},
            { title: "Designation", data: "employeeDesignation"},
            { title: "Status", data:"employeeStatus"}
        ],
        columnDefs: [
            {
                targets: [ 0 ],
                visible: false,
                searchable: false
            },
            {
                targets: [ 2 ],
                visible: false,
                searchable: false
            },
            {
                targets: [ 7 ],
                visible: false,
                searchable: false
            }
        ]
    } );

    // table click event
    $('#employeeTable tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        fillFormData(data);
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    } );



    $( "#employeeSave" ).click(function() {
        var employeeDTO = {};
        employeeDTO.employeeId = $( "#employeeId" ).val();
        employeeDTO.employeeFirstName = $( "#employeeFirstName" ).val();
        employeeDTO.employeeLastName = $( "#employeeLastName" ).val();
        employeeDTO.employeeNIC = $( "#employeeNIC" ).val();
        employeeDTO.employeeEPF = $( "#employeeEPF" ).val();
        employeeDTO.employeeAddress = $( "#employeeAddress" ).val();
        employeeDTO.employeeMobileNumber = $( "#employeeMobileNumber" ).val();
        employeeDTO.employeeEmail = $( "#employeeEmail" ).val();
        employeeDTO.employeeBasicSalary = $( "#employeeBasicSalary" ).val();
        employeeDTO.employeeAllowance = $( "#employeeAllowance" ).val();
        employeeDTO.employeeDesignation = $( "#employeeDesignation" ).val();
        employeeDTO.employeeStatus = $("#employeeStatus").val();

        $.ajax({
            url : '/employee/saveEmployees', // or whatever
            dataType : 'json',
            type: 'post',
            data: employeeDTO,
            success : function (response) {
                if(response.success){
                    alert("Successfully Saved the Employee ");
                    employeeClear(true);
                } else {
                    alert("Error Saving Employee  !!!");
                }
            }
        })
        ;
    });


    $( "#employeeUpdate" ).click(function() {
        var employeeDTO = {};
        employeeDTO.employeeId = $( "#employeeId" ).val();
        employeeDTO.employeeFirstName = $( "#employeeFirstName" ).val();
        employeeDTO.employeeLastName = $( "#employeeLastName" ).val();
        employeeDTO.employeeNIC = $( "#employeeNIC" ).val();
        employeeDTO.employeeEPF = $( "#employeeEPF" ).val();
        employeeDTO.employeeAddress = $( "#employeeAddress" ).val();
        employeeDTO.employeeMobileNumber = $( "#employeeMobileNumber" ).val();
        employeeDTO.employeeEmail = $( "#employeeEmail" ).val();
        employeeDTO.employeeBasicSalary = $( "#employeeBasicSalary" ).val();
        employeeDTO.employeeAllowance = $( "#employeeAllowance" ).val();
        employeeDTO.employeeDesignation = $( "#employeeDesignation" ).val();
        employeeDTO.employeeStatus = $("#employeeStatus").val();

        $.ajax({
            url : '/employee/updateEmployee', // or whatever
            dataType : 'json',
            type: 'post',
            data: employeeDTO,
            success : function (response) {
                if(response.success){
                    alert("Successfully Updated the Employee ");
                    employeeClear(true);
                } else {
                    alert("Error Updating Employee  !!!");
                }
            }
        })
        ;
    });

    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $( "#employeeUpdate" ).hide();
    $( "#employeeSave" ).show();

});

function employeeClear(disable) {
    $("#employeeDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#employeeUpdate" ).hide();
    $( "#employeeSave" ).show();

}

function employeeAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#employeeSave" ).show();
    $( "#employeeUpdate" ).hide();

}

function resetForm() {
    $(':input','#employeeForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#employeeId").prop('disabled', disable);
    $("#employeeFirstName").prop('disabled', disable);
    $("#employeeLastName").prop('disabled', disable);
    $("#employeeNIC").prop('disabled', disable);
    $("#employeeEPF").prop('disabled', disable);
    $("#employeeAddress").prop('disabled', disable);
    $("#employeeMobileNumber").prop('disabled', disable);
    $("#employeeEmail").prop('disabled', disable);
    $("#employeeBasicSalary").prop('disabled', disable);
    $("#employeeAllowance").prop('disabled', disable);
    $("#employeeDesignation").prop('disabled', disable);
    $("#employeeStatus").prop('disabled', disable);


}

function fillFormData(row) {
    $("#employeeId").val(row.employeeId);
    $("#employeeFirstName").val(row.employeeFirstName);
    $("#employeeLastName").val(row.employeeLastName);
    $("#employeeNIC").val(row.employeeNIC);
    $("#employeeEPF").val(row.employeeEPF);
    $("#employeeAddress").val(row.employeeAddress);
    $("#employeeMobileNumber").val(row.employeeMobileNumber);
    $("#employeeEmail").val(row.employeeEmail);
    $("#employeeBasicSalary").val(row.employeeBasicSalary);
    $("#employeeAllowance").val(row.employeeAllowance);
    $("#employeeDesignation").val(row.employeeDesignation);
    $("#employeeStatus").val(row.employeeStatus);
}


function disableUpdate(disable) {
    $("#employeeUpdateButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#employeeDeleteButton").prop('disabled', disable);

}
// Edit Button
function employeeUpdate(){
    if(clickedRow){
        disableFormData(true);
        $("#employeeLastName").prop('disabled', false);
        $("#employeeAddress").prop('disabled', false);
        $("#employeeMobileNumber").prop('disabled', false);
        $("#employeeEmail").prop('disabled', false);
        $("#employeeBasicSalary").prop('disabled', false);
        $("#employeeAllowance").prop('disabled', false);
        $("#employeeDesignation").prop('disabled', false);
        $("#employeeStatus").prop('disabled', false);
        $( "#employeeUpdate" ).show();
        $( "#employeeSave" ).hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}

function employeeDelete(){
    if(clickedRow){
        if (confirm("Are you sure to delete the selected employee") == true) {
            var employeeDTO = {};
            employeeDTO.employeeId = $( "#employeeId" ).val();
            employeeDTO.employeeFirstName = $( "#employeeFirstName" ).val();
            employeeDTO.employeeLastName = $( "#employeeLastName" ).val();
            employeeDTO.employeeNIC = $( "#employeeNIC" ).val();
            employeeDTO.employeeEPF = $( "#employeeEPF" ).val();
            employeeDTO.employeeAddress = $( "#employeeAddress" ).val();
            employeeDTO.employeeMobileNumber = $( "#employeeMobileNumber" ).val();
            employeeDTO.employeeEmail = $( "#employeeEmail" ).val();
            employeeDTO.employeeBasicSalary = $( "#employeeBasicSalary" ).val();
            employeeDTO.employeeAllowance = $( "#employeeAllowance" ).val();
            employeeDTO.employeeDesignation = $( "#employeeDesignation" ).val();
            employeeDTO.employeeStatus = $("#employeeStatus").val();

            $.ajax({
                url : '/employee/deleteEmployee', // or whatever
                dataType : 'json',
                type: 'post',
                data: employeeDTO,
                success : function (response) {
                    if(response.success){
                        alert("Successfully deleted the Employee ");
                        employeeClear(true);
                    } else {
                        alert("Error while deleting Employee  !!!");
                    }
                }
            })
            ;
        }

    } else {
        alert("Please Select a Row first !!!!");
    }
}

function searchEmployee(){
    table.ajax.reload();
}