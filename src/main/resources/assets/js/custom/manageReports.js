/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */

var table;
var table1;
var table2;

$(document).ready(function () {


    var table = $('#reportsTableBooking').DataTable( {



        processing: true,
        ajax: {
            url: "/reports/getAllEngs",
            "type": "GET",
            dataSrc: function (json) {

                console.log(json.tableData);
                if(json.tableData.length > 0 ){

                    json.tableData.forEach(function(row) {
                        console.log(row);
                        var sDate = row.startDate;
                        row.startDate = new Date(sDate).toString().substr(0,15);
                        var eDate = row.endDate;
                        row.endDate = new Date(eDate).toString().substr(0,15);
                        var rDate = row.reservationRequestDate;
                        row.reservationRequestDate = new Date(rDate).toString().substr(0,15);
                    });

                    return json.tableData;
                }
                return [];
            }
        },

        columns: [

            {title: "Booking ID", data: 'bookingId'},
            {title: "Request Date", data: 'reservationRequestDate'},
            {title: "Start Date", data: 'startDate'},
            {title: "End Date", data: 'endDate'},
            {title: "Location", data: 'eventLocation'},
            {title: "Programme Title", data: 'titleOfProgramme'},
            {title: "Status", data: 'bookingStatus'}/*,
            {title: "Edit"}*/
        ],
        columnDefs: [

            {

                targets: [7],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm " type="button" onclick="selectButtonPm(this)">Select</button>'
                }

            }
        ],

        order: [[1, 'asc']]
    } );



});


function selectButtonPm(obj) {

    var bookingId = $(obj).closest('tr').find('td:first').html();

    table2 = $('#reportsTableBookingItem').DataTable({
        processing: true,
        dom: 'Bfrtip',
        ajax: {
            url: "/reports/getBookingByIdForReports?bookingId="+bookingId,
            dataType: 'json',
            type: 'get',
            dataSrc: function (json) {


                console.log(json.tableData);
                if(json.tableData.length > 0 ){

                    json.tableData.forEach(function(row) {
                        console.log(row);
                        var sDate = row.startDate;
                        row.startDate = new Date(sDate).toString().substr(0,15);
                        var eDate = row.endDate;
                        row.endDate = new Date(eDate).toString().substr(0,15);
                        var rDate = row.reservationRequestDate;
                        row.reservationRequestDate = new Date(rDate).toString().substr(0,15);
                    });

                    return json.tableData;
                }
                return [];
            }
        },
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print',
        ],
        columns: [

            {title: "Booking ID", data: 'bookingId'},
            {title: "Request Date", data: 'reservationRequestDate'},
            {title: "Start Date", data: 'startDate'},
            {title: "End Date", data: 'endDate'},
            {title: "Location", data: 'eventLocation'},
            {title: "Programme Title", data: 'titleOfProgramme'},
            {title: "Status", data: 'bookingStatus'}
        ],

        order: [[1, 'asc']],
        destroy: ['true']

    } );

    table1 = $('#reportsTableItems').DataTable({
        processing: true,
        dom: 'Bfrtip',
        ajax: {
            url: "/reports/getAllEngItems?bookingId="+bookingId,
            dataType: 'json',
            type: 'get',
            dataSrc: function (json) {


                console.log(json.tableData);
                return json.tableData;
            }
        },
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        columns: [

            {title: "Inventory ID", data: 'inventoryId'},
            {title: "Type", data: 'type'},
            {title: "Sub Type", data: 'subType'},
            {title: "Serial", data: 'serial'}
        ],

        order: [[1, 'asc']],
        destroy: ['true']

    } );

    return false;

}




function itemsReset() {

    window.location.reload();
}



/**
 * Created by RAVI KALUARACHCHI on 8/27/2019.
 */
