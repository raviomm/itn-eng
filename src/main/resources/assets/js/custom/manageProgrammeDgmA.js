/**
 * Created by RAVI KALUARACHCHI on 7/2/2018.
 */
var originalInvMap = [];
var addedInvMap = [];
var inventoryObj = {};
var table;
var table1;
var bookingIdForComment;

$(document).ready(function () {


    var table = $('#engProgrammeApproval').DataTable( {


        /*ajax: "/engPApproval/getAllEngs",*/
        processing: true,
        ajax: {
            url: "/engPApproval/getAllEngs",
            "type": "GET",
            dataSrc: function (json) {

                console.log(json.tableData);
                if(json.tableData.length > 0 ){
                    json.tableData.forEach(function(row) {
                        console.log(row);
                        var sDate = row.startDate;
                        row.startDate = new Date(sDate).toString().substr(0,15);
                        var eDate = row.endDate;
                        row.endDate = new Date(eDate).toString().substr(0,15);
                        var rDate = row.reservationRequestDate;
                        row.reservationRequestDate = new Date(rDate).toString().substr(0,15);
                    });
                    return json.tableData;
                }
                return [];
            }
        },

        columns: [

            {title: "Booking ID", data: 'bookingId'},
            {title: "Request Date", data: 'reservationRequestDate'},
            {title: "Start Date", data: 'startDate'},
            {title: "End Date", data: 'endDate'},
            {title: "Location", data: 'eventLocation'},
            {title: "Programme Title", data: 'titleOfProgramme'},
            {title: "Status", data: 'bookingStatus'},
            {title: "Select" },
            {title: "Approval" }
        ],
        columnDefs: [

            {

                targets: [7],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm " type="button" onclick="selectButtonPm(this)">Select</button>'
                }

            }
            ,{

                targets: [8],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-success btn-fill btn-sm" onclick="statusApprovePm(this)">Approve</button>&nbsp&nbsp&nbsp<br><button id="' + row.id +'" class="btn btn-danger btn-fill btn-sm" onclick="statusCancelPm(this)">Cancel</button>'
                }

            }
        ],
        order: [[1, 'asc']]
    } );

    // table click event
    $('#engProgrammeApproval tbody').on('click', 'tr', function () {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }

    });

    disableSubmitButton(true);
    disableTextarea(true);
    disableAllCancelButtons(true);



});




function selectButtonPm(obj) {

    disableAllCancelButtons(true);
    disableSubmitButton(false);
    disableTextarea(false);



    var bookingId = $(obj).closest('tr').find('td:first').html();
    bookingIdForComment = bookingId;

    table1 = $('#engProgrammeApprovalItems').DataTable({
        processing: true,
        ajax: {
            url: "/engPApproval/getAllEngItems?bookingId="+bookingId,
            dataType: 'json',
            type: 'get',
            dataSrc: function (json) {
                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [

            {title: "Inventory ID", data: 'inventoryId'},
            {title: "Type", data: 'type'},
            {title: "Sub Type", data: 'subType'},
            {title: "Serial", data: 'serial'}
        ],

        order: [[1, 'asc']],
        destroy: ['true']

    } );

    return false;
}


// Approve Button function

function statusApprovePm(obj,bookingId) {

    var bookingformDTO = {};
    bookingId = $(obj).closest('tr').find('td:first').html();
    bookingformDTO.bookingId = bookingId;

    $.ajax({
        url: "/engPApproval/statusApprovePm", // or whatever
        dataType: 'json',
        type: 'post',
        data:bookingformDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Approved by Programme Controller ");
            } else {
                alert("Error Updating Reservation  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });

}




//Cancel Button Function

function statusCancelPm(obj,bookingId) {



    var bookingformDTO = {};
    bookingId = $(obj).closest('tr').find('td:first').html();
    bookingIdForComment = bookingId;
    bookingformDTO.bookingId = bookingId;


    $.ajax({
        url: "/engPApproval/statusCancelPm", // or whatever
        dataType: 'json',
        type: 'post',
        data:bookingformDTO,
        success: function(response) {
            if (response.success) {
                alert("ENG Booking Canceled ");
            } else {
                alert("Error Cancelling Reservation  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });

}

function commentSubmitfunction() {

    disableAllCancelButtons(false);
    disableAllApproveButtons(true);

    var bookingId =bookingIdForComment;
    var bookingCommentsDTO = {};
    bookingCommentsDTO.bookingId = bookingId;
    bookingCommentsDTO.comment = $("#comment").val();

    $.ajax({

       url:"/engPApproval/commentSubmit",
        dataType: 'json',
        type: 'post',
        data: bookingCommentsDTO,
        success: function(response) {
            if (response.success) {
                alert("comment submit complete ");
            } else {
                alert("comment submit not complete  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }




    });




}

function disableSubmitButton(disable) {

    if (disable == true){
        document.getElementById("commentSubmit").disabled = true;
    }
    else{
        document.getElementById("commentSubmit").disabled = false;
    }

}

    function disableTextarea(disable) {

        if (disable == true) {
            document.getElementById("comment").disabled = true;
        }
        else {
            document.getElementById("comment").disabled = false;
        }

    }


function disableAllCancelButtons(disable) {


    $('.btn-danger').attr('disabled', disable);

}

function disableAllApproveButtons(disable) {


    $('.btn-success').attr('disabled', disable);

}


